import os;
import sys;
import networkx as nx;


nodes_label_mapping={}; # Declared globally
count_label_id=0;

amino_acid_dict = {}
for line in open("aminoacid_dict.txt"):
    amino_acid_dict[line.strip()] = 1


class   Node:
    def __init__(self, res_id, res_name,chain_name):
        self.residue_id_=res_id;
        self.residue_name_=res_name.strip();
        self.chain_name_=chain_name.strip();

    def __eq__(self, other):
        if  (self.residue_id_,self.residue_name_,self.chain_name_) == (other.residue_id_,other.residue_name_,other.chain_name_):
            return True;
        else:
            return False;

    def __lt__(self,other):
        return self.residue_id_ < other.residue_id_;

    def __gt__(self,other):
        return self.residue_id_ > other.residue_id_;

    def __hash__(self):
        return hash ((self.residue_id_,self.residue_name_,self.chain_name_));

    def __str__(self):
        return str(self.residue_id_)+","+str(self.residue_name_)+str(",")+str(self.chain_name_);


class   EdgePair:
    def __init__(self, node_1, node_2,distance):
        self.node_1_= node_1;
        self.node_2_= node_2;
        self.distance_ =distance;

    def __eq__(self, other):
        return ((self.node_1_,self.node_2_) == (other.node_1_,other.node_2_)) or ((self.node_2_, self.node_1_)== (other.node_1_, other.node_2_));
                                        
          
    def __lt__(self,other):
        return (self.node_1_,self.node_2_) < (other.node_1_, other.node_2_);

    def __gt__(self, other):
        return (self.node_1_,self.node_2_) > (other.node_1_, other.node_2_);

    def __hash__(self):
        return hash ((self.node_1_, self.node_2_));

    def __str__(self):
        return str(self.node_1_)+","+str(self.node_2_)+","+str(self.distance_);





def processEdges (set_of_edges,set_of_nodes,graphid,graph_file,graph_name):
    global nodes_label_mapping;
    global count_label_id;

    if len(set_of_nodes) == 0:
        return ;

    nodes_mapping={};
    count_id = 0;   
    for k   in  sorted(set_of_nodes.keys()):
        nodes_mapping[k]=count_id;
        if  k.residue_name_ not in nodes_label_mapping:
            nodes_label_mapping[k.residue_name_]=count_label_id;    
            count_label_id=count_label_id + 1;
        count_id=count_id+1;
    # Print node mapping

    nodes_mapping_dir="nodemappings/";

    if  not (os.path.exists(nodes_mapping_dir)):
        os.makedirs (nodes_mapping_dir,0o777);

    label_mapping_dir="labelmappings/";
    if  not (os.path.exists(label_mapping_dir)):
        os.makedirs (label_mapping_dir,0o777);   

    graph_file.write("t"+" #"+" "+str(int(graphid) -1)+os.linesep);
    filetowrite=open(nodes_mapping_dir+str(graphid)+"_"+str(graph_name)+"_nodes_mapping.txt","w");
    for v   in  sorted (nodes_mapping, key=nodes_mapping.get):
        graph_file.write("v"+" "+str(nodes_mapping[v])+" "+str(nodes_label_mapping[v.residue_name_])+os.linesep);
        filetowrite.write (str(nodes_mapping[v])+":"+str(v)+os.linesep);    

    # Print label mapping
    
    filelabel=open(label_mapping_dir+"label_mapping.txt","w");
    for v   in sorted (nodes_label_mapping, key=nodes_label_mapping.get):
        filelabel.write(str(nodes_label_mapping[v])+":"+str(v)+os.linesep);

    set_of_nodes.clear();

    # Enter into a another edge dictionary; then sort it
    set_of_final_edges={};

    for k   in  set_of_edges.keys():
        #print (str(k));
        n1=Node ( int (nodes_mapping[k.node_1_]), "Default","Default");
        n2=Node ( int (nodes_mapping[k.node_2_]), "Default","Default");
        e =EdgePair (n1,n2,0.0);
        set_of_final_edges [e]= 1;

    # Check again if the graph is connected or not
    G=nx.Graph();
    for k   in  set_of_final_edges.keys():
        G.add_edge(str(k.node_1_),str(k.node_2_));
    
    if  nx.is_connected(G) == False:
        print ("Problem");
        sys.exit();

    #print len(nx.connected_components[0]);

    for k   in  sorted (set_of_final_edges.keys()):
        graph_file.write("e "+str(k.node_1_.residue_id_)+ " "+ str(k.node_2_.residue_id_)+" "+str(1)+os.linesep);


def getconnected_edges(set_of_edges):
    G=nx.Graph();
    set_of_nodes={};
    for k   in  set_of_edges.keys():
        G.add_edge(str(k.node_1_),str(k.node_2_));


    conn_components = sorted(nx.connected_components(G), key = len, reverse=True)
    if len(conn_components) == 0:
        return {}, {};

    component = list(conn_components[0])
    print ("comp=%i"%len(component))

    for pos in  range(0,len(component)):       
        nodestring = component[pos];
        nodestringvalues=nodestring.split(",");
        n= Node (int (nodestringvalues[0]), nodestringvalues[1], nodestringvalues [2]);
        if  n   not in  set_of_nodes:
            set_of_nodes[n]=1;  

    set_of_connected_edges={};

    #has redundant checking
    for k   in  sorted(set_of_edges.keys()):
        if  k.node_1_ in    set_of_nodes and k.node_2_ in   set_of_nodes:
            set_of_connected_edges[k] =1;   

    return set_of_connected_edges,set_of_nodes;

def parseDistanceFile (filetoread):
    #for each graph do
    count =0;
    graph_name="";
    chain_1="";
    chain_2="";
    set_of_edges_={};
    set_of_nodes_={};
    graph_file =open("graph.txt","w");
    for line    in  filetoread:
        if  line.startswith("#"):
            if  count==0:
                count =count +1;
                graph_name=line.strip()[line.strip().find("#")+1:];
                continue;
            else:
                set_of_connected_edges_,set_of_connected_nodes_= getconnected_edges(set_of_edges_);
                if len(set_of_connected_nodes_) < 16:
                    print ("Number of nodes is not sufficient")
                else:
                    processEdges(set_of_connected_edges_,set_of_connected_nodes_,count,graph_file,graph_name);
                set_of_edges_={};
                set_of_nodes_={};
                set_of_connected_edges_.clear();
                set_of_connected_nodes_.clear();
                graph_name=line.strip()[line.strip().find("#")+1:];
                count=count+1;
                continue;
                # write the data
                # initialize edge dictionary

        if  line.startswith("Chain"):
            #print count;
            #print graph_name;
            line=line.strip();
            chains=line.split(":");
            chain_1=chains[1];
            chain_2=chains[2];
            #print chains;

        else:
            line =line.strip();
            values=line.split(",");
            n1= Node ( int (values[0]),values[3], chain_1);
            n2= Node ( int (values[1]),values[4], chain_2);
            #print (str(n1));
            #print (str(n2));
            #if     n1  not in  set_of_nodes_.keys():
            #   set_of_nodes_[n1]=1;
            #if n2  not in  set_of_nodes_.keys():
            #   set_of_nodes_[n2]=1;

            if values[3] not in amino_acid_dict or values[4] not in amino_acid_dict:
                continue

            e= EdgePair(n1,n2,values[2]);


            if  e   in  set_of_edges_.keys():
                continue;
            else:
                set_of_edges_ [e] =1;
        
        
    set_of_connected_edges_,set_of_connected_nodes_= getconnected_edges(set_of_edges_);
    processEdges(set_of_connected_edges_,set_of_connected_nodes_,count,graph_file,graph_name);

parseDistanceFile (open(sys.argv[1]));
