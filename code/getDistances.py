import os;
import sys;
import os.path;
from Bio import PDB;
from math import sqrt;


#@param 1 : pdb_id_file
#@param 2 : data_dir

pdb_id_file=open(sys.argv[1]);
data_dir=sys.argv[2];

intra_chain_distance = 0.0;
inter_chain_distance = 12.0;

# Based on the discussion with Dr. Hasan, I here consider inter chain edges 
# between residues if their distance is within 8.0A and intra chain edges
# are considered only for those residues which are in the set of 
# vertices participating in inter chain edges.

inter_chain_residue_dict = {};

def getpdblist ():
    pdblist=[];
    for line    in  pdb_id_file:
        line =line.strip();
        pdbs =line.split(",");
        for pos in  range(0,len(pdbs)):
            if  len(str(pdbs[pos].strip()))== 4:
                pdblist.append(pdbs[pos]);

    return pdblist;


def getDistance(point1,point2):
    distance =point1 - point2;
    sqr_dst=sqrt(distance[0]*distance[0] + distance[1]*distance[1] + distance[2]*distance[2]);
    return sqr_dst;


def processResidues (residue_1,residue_2,distance, filetowrite):
    filetowrite.write(str(residue_1.get_id()[1])+","+str(residue_2.get_id()[1])+","+str(distance)+","+residue_1.get_resname()+","+
                residue_2.get_resname()+os.linesep);

def checkIfIninterchainedgevertices(residue_,chain_name):
    global inter_chain_residue_dict ;   
    if  str(residue_.get_id()[1])+chain_name not in inter_chain_residue_dict:
        return False;
    else:
        return True;
    
def generateEdges_intra_chain ( distancelimit,list_of_chain,filetowrite):
    #working with only two chain
    global inter_chain_residue_dict ;
    for residue_1   in  list_of_chain[0]:
        if  residue_1.has_id ("CA"):
            point1=residue_1["CA"].get_coord();
            for residue_2   in  list_of_chain[1]:
                if  residue_2.has_id("CA"):
                    if  int(residue_1.get_id()[1]) == int(residue_2.get_id()[1]):
                        continue;
                    if  int(residue_1.get_id()[1])> int(residue_2.get_id()[1]):
                        continue;                       
                    point2=residue_2["CA"].get_coord();
                    sqr_dst=getDistance(point1,point2);
                    if  sqr_dst <=distancelimit:
                        val1=checkIfIninterchainedgevertices (residue_1,list_of_chain[0].get_id());
                        val2=checkIfIninterchainedgevertices (residue_2,list_of_chain[1].get_id());
                        if  val1==True and val2==True:
                            processResidues(residue_1,residue_2,sqr_dst,filetowrite);   
                    else:
                        continue;



def insertIntoResidueDictionary(residue_,chain_name):
    global inter_chain_residue_dict ;
    if  str(residue_.get_id()[1])+chain_name not in inter_chain_residue_dict:
        inter_chain_residue_dict [str(residue_.get_id()[1])+chain_name]=1;


    
def generateEdges_inter_chain ( distancelimit,list_of_chain,filetowrite):
    global inter_chain_residue_dict;
    #working with only two chain
    for residue_1   in  list_of_chain[0]:
        if  residue_1.has_id ("CA"):
            point1=residue_1["CA"].get_coord();
            for residue_2   in  list_of_chain[1]:
                if  residue_2.has_id("CA"):
                    point2=residue_2["CA"].get_coord();
                    sqr_dst=getDistance(point1,point2);
                    if  sqr_dst <=distancelimit:
                        insertIntoResidueDictionary (residue_1,list_of_chain[0].get_id());
                        insertIntoResidueDictionary (residue_2,list_of_chain[1].get_id());
                        processResidues (residue_1,residue_2,sqr_dst,filetowrite);  
                    else:
                        continue;



def processTwoChainPdbs(pdblist,filetowrite):
    parser=PDB.PDBParser(PERMISSIVE=1);
    chainstat_file=open("chain-stat.txt","w");
    global inter_chain_residue_dict ;
    for pos in  range(0,len(pdblist)):
        filetoopen=data_dir +"/"+pdblist[pos]+".ent";
        if  not(os.path.isfile(filetoopen)):
            filetoopen=os.path.join(data_dir,"%s%s"%(pdblist[pos],".pdb"))
            if not(os.path.exists(filetoopen)):
               filetoopen=os.path.join(data_dir,"%s%s"%(pdblist[pos].lower(),".pdb"))

        if not(os.path.exists(filetoopen)):
            continue
        structure=parser.get_structure(pdblist[pos], filetoopen)
        filetowrite.write ("#"+str(structure.get_id()) + os.linesep);
        
        list_of_chain=[];
        for chain in  structure[0]:
                list_of_chain.append(chain);
            
        
        if len(list_of_chain) < 2:
            continue; 

        chainstat_file.write(str(pdblist[pos])+" : "+str(list_of_chain)+os.linesep);
        #chainstat_file.write(str(pdblist[pos])+os.linesep);

       
        #interchain
        filetowrite.write ("Chain:"+str(list_of_chain[0].get_id())+":"+str(list_of_chain[1].get_id())+os.linesep);
        generateEdges_inter_chain  (inter_chain_distance,list_of_chain, filetowrite);

        #intrachain
        list_of_reverse_chain=[];
        list_of_reverse_chain.append(list_of_chain[1]);
        list_of_reverse_chain.append(list_of_chain[1]);
        filetowrite.write ("Chain:"+str(list_of_reverse_chain[0].get_id())+":"+str(list_of_reverse_chain[1].get_id())+os.linesep);
        generateEdges_intra_chain (intra_chain_distance,list_of_reverse_chain, filetowrite);

        #intrachain
        list_of_reverse_chain=[];
        list_of_reverse_chain.append(list_of_chain[0]);
        list_of_reverse_chain.append(list_of_chain[0]);
        filetowrite.write ("Chain:"+str(list_of_reverse_chain[0].get_id())+":"+str(list_of_reverse_chain[1].get_id())+os.linesep);
        generateEdges_intra_chain (intra_chain_distance,list_of_reverse_chain, filetowrite);

        inter_chain_residue_dict.clear();
        inter_chain_residue_dict ={};

        filetowrite.flush()
        chainstat_file.flush()

            
pdblist=getpdblist();
filetowrite=open("Distances.txt","w");
processTwoChainPdbs(pdblist,filetowrite);
