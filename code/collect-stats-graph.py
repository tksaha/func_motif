import os;
import sys;
import networkx as nx;
import math;
from scipy import stats;

graph_file=open(sys.argv[1]);
count=0;
G=nx.Graph();
edge_length_list=[];
node_length_list=[];
false_count=0;

for	line	in	graph_file:
	if	line.startswith("t"):
		if	count==0:
			count=count+1;
			continue;
		else:
			if	nx.is_connected(G)==False:
				false_count=false_count+1;
				continue;
			edge_length_list.append(int(G.number_of_edges()));	
			node_length_list.append(int(G.number_of_nodes()));
			G=nx.Graph();
	if	line.startswith("e"):
		line=line.strip();
		nodes=line.split(" ");
		G.add_edge(int(nodes[1]), int(nodes[2]))
if	nx.is_connected(G)==False:
	false_count=false_count+1;

else:
	edge_length_list.append(int(G.number_of_edges()));	
	node_length_list.append(int(G.number_of_nodes()));

print ("Printing edge counts:")
for	pos	in	range(0,len(edge_length_list)):
	print (str(pos+1)+"->"+str(edge_length_list[pos]));

print ("printing node counts:")
for	pos	in	range(0,len(node_length_list)):
	print (str(pos+1)+"->"+str(node_length_list[pos]))


size,min_max,mean,variance,skew,kurt = stats.describe (node_length_list);
print ("Node Stats: "+"Mean: "+str(mean)+" std. dev: "+str(math.sqrt(variance))+" Min:"+str(min_max[0])+" Max:"+str(min_max[1]));

size,min_max,mean,variance,skew,kurt = stats.describe (edge_length_list);
print ("Edge Stats: "+"Mean: "+str(mean)+" std. dev: "+str(math.sqrt(variance))+" Min:"+str(min_max[0])+" Max:"+str(min_max[1]));

print ("Multicomponent Count: "+str(false_count));
print ("No of Graphs:"+ str(len(edge_length_list)));
#print "avg edge count: "+ str(sum(edge_length_list)/len(edge_length_list));
