import os;
import sys;

import networkx as nx;
import networkx.algorithms.isomorphism as iso;


#@param 1 : graph file
#@param 2 : patternfile

def	readPatternsAndGetList (patternFile):
	patternList =[];
	for	line	in	patternFile:
		line=line.strip();
		edgeseq=line[line.find ("#")+1: line.rfind ("#")];
		patternList.append(edgeseq);

	return patternList;

def	insertEdges (edgeelems_,set_of_nodes,set_of_edges):
	st = edgeelems_[0]; end = edgeelems_[1];
	stl =edgeelems_[2]; endl =edgeelems_[3];
	edgel = edgeelems_[4];

	if	st	not	in	set_of_nodes.keys():
			set_of_nodes[st]=stl;

	if	end	not	in	set_of_edges.keys():
			set_of_nodes[end]=endl;

	edgetuple = (st,end);

	if	edgetuple not	in	set_of_edges.keys():
		set_of_edges [edgetuple]=edgel;



def	readBigGraphSetAndGetsetofnodesandEdges (graphFile):

	set_of_graphs_nodes_and_edges = [];
	set_of_nodes={};
	set_of_edges={};
	
	for	line	in	graphFile:
		if	"t"	in	line:
			if	len(set_of_nodes)> 0 and len(set_of_edges) >0:
				set_of_graphs_nodes_and_edges.append( (set_of_nodes,set_of_edges));
				print str(len(set_of_nodes)) +"," + str(len(set_of_edges));
				set_of_nodes={};
				set_of_edges={};

		else:
			#print line;
			if	"v"	in	line:
				lineelems = line.strip().split(" ");
				#print lineelems;
				st =	int (lineelems [1]);
				stl = int (lineelems [2]);
				if	st	not	in	set_of_nodes:
					set_of_nodes [st] = stl;
			
			if	"e"	in	line:
				lineelems= line.strip().split (" ");
				st = int (lineelems [1]);
				end = int (lineelems [2]);
				edgel = int (lineelems [3]);

				edgetuple = (st,end);
				if	edgetuple not in	set_of_edges:
					set_of_edges [edgetuple] =edgel;

	if	len(set_of_nodes)> 0 and len(set_of_edges) >0:
		set_of_graphs_nodes_and_edges.append( (set_of_nodes,set_of_edges));
		print str(len(set_of_nodes)) +"," + str(len(set_of_edges));
		set_of_nodes={};
		set_of_edges={};


	print len(set_of_graphs_nodes_and_edges); 
	return set_of_graphs_nodes_and_edges;


def	createGraph (set_of_nodes, set_of_edges ):
	g=nx.Graph();
	for	k,v	in	sorted (set_of_nodes.items()):
		g.add_node (k,vlabel=v);

	for	k,v	in	sorted (set_of_edges.items()):
		g.add_edge ( k[0], k[1],elabel=v);

	return g;


def	patternToSetOfNodesAndEdges (pattern):
	patternElems_  = pattern.strip().split (")");
	set_of_nodes ={};
	set_of_edges ={};
	for	patterns_	in	patternElems_:
		if	len(patterns_) ==0:
			continue;
		edgeelements_ = patterns_.split(",");
		#print edgeelements_;
		firstnode= edgeelements_[0].split("(");
		st = int (firstnode[1]);
		end = int (edgeelements_[1]);
		stl =int (edgeelements_ [2]);
		edgel=int (edgeelements_ [3]);
		endl =int (edgeelements_ [4]);
		edgeelems_=[];
		edgeelems_.append(st); edgeelems_.append(end);
		edgeelems_.append(stl); edgeelems_.append(endl);
		edgeelems_.append(edgel);
		insertEdges (edgeelems_,set_of_nodes,set_of_edges);

	return set_of_nodes, set_of_edges;



def	createBigGraphList (set_of_graphs_nodes_and_edges):
	biggraphlist =[];
	for	pos	in	range (0,len(set_of_graphs_nodes_and_edges)):
		biggraph_ =createGraph (set_of_graphs_nodes_and_edges [pos] [0],set_of_graphs_nodes_and_edges [pos] [1] );
		biggraphlist.append(biggraph_);

	return biggraphlist;


# Param 2 : Pattern File
patternList_ = readPatternsAndGetList (open(sys.argv[2]));

# Param 1 : Graph File
set_of_graphs_nodes_and_edges = readBigGraphSetAndGetsetofnodesandEdges (open(sys.argv[1]));
biggraphList = createBigGraphList (set_of_graphs_nodes_and_edges);

filetowrite=open ("edgeseq.txt","w");

for	pattern_	in	patternList_:
	for	pos	in	range (0,len(biggraphList)):
		set_of_nodes_,set_of_edges_ = patternToSetOfNodesAndEdges (pattern_);
		smallgraph_= createGraph (set_of_nodes_,set_of_edges_);
		nodematch=iso.numerical_node_match('vlabel',1);
		edgematch=iso.numerical_edge_match('elabel',1);
		instance=iso.GraphMatcher(biggraphList[pos],smallgraph_,nodematch,edgematch);
		if 	instance.subgraph_is_isomorphic():
			for	mapping_ in	instance.subgraph_isomorphisms_iter():
				node_dict ={};
				filetowrite.write (str(pos+1)+":"+pattern_+":");
				for	k,v	in	mapping_.items():
					node_dict [v]=k;
				for	edges_	in	smallgraph_.edges():
					filetowrite.write ("("+str(node_dict[edges_[0]])+","+str(node_dict[edges_[1]])+")");

				filetowrite.write (os.linesep);

			print "found";



