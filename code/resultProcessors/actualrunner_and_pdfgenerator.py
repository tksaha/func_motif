import os;
import sys;
import subprocess;


filedict={};


def	sortfiles():
	global filedict;
	# pattern directory
	patterndir=sys.argv[1];
	allfiles_in_dir=os.listdir(patterndir);
	filedict = {};

	#get the position
	for	pos	in	range(0,len(allfiles_in_dir)):
		file_ = str(allfiles_in_dir[pos]);
		pattern_no = int ( file_[file_.find("_")+1: file_.find("_residue")]);
		filedict[file_] = pattern_no;

	
	

def	doActualMapping():
	patterndir=sys.argv[1];
 	os.chdir(patterndir);	

	filetowrite = open ("../actual/allpatternresidues.txt","w");
	filetowrite.close();

	global filedict;
	pos=0;
	# Done actual mapping and tex generation
	for	file_	in	sorted(filedict, key=filedict.get):
		filetorun=file_;
		filetowrite_name = "../actual/allpatternresidues.txt";
		filetowritex =open (filetowrite_name,"a");
		filetowritex.write ( "--------- Pattern No. "+str(pos+1)+"---------"+os.linesep); 
		filetowritex.close();
		pos =pos +1;
		args=["python","../patternactualmapper.py", "../../../allpdbs.txt",filetorun,filetowrite_name];
		p=subprocess.Popen(args);
		p.communicate();



def	generateTex():
	os.chdir ("../tex/");
	allfiles_= os.listdir(os.getcwd());

	for	pos	in	range (0,len(allfiles_)):
		if	".tex" 	in	allfiles_[pos]:
			args=["latex",allfiles_[pos]];
			p=subprocess.Popen(args,stdout=subprocess.PIPE,stderr=subprocess.PIPE);
			out,err=p.communicate();
			#print out;
			#print err;

def	generatePS():
	allfiles_= os.listdir(os.getcwd());
	for	pos	in	range (0,len(allfiles_)):
		if	".dvi" 	in	allfiles_[pos]:
			args=["dvips", allfiles_[pos]];
			subprocess.Popen(args);
			p=subprocess.Popen(args,stdout=subprocess.PIPE,stderr=subprocess.PIPE);
			out,err=p.communicate();
			#print out,err;

def	generatePDF():
	allfiles_= os.listdir(os.getcwd());
	for	pos	in	range (0,len(allfiles_)):
		if	".ps" 	in	allfiles_[pos]:
			args=["ps2pdf", allfiles_[pos]];
			subprocess.Popen(args);
			p=subprocess.Popen(args,stdout=subprocess.PIPE,stderr=subprocess.PIPE);
			out,err=p.communicate();


def	removeunncessaryFiles():
	allfiles_= os.listdir(os.getcwd());
	for	pos	in	range (0,len(allfiles_)):
		if	".ps" in	allfiles_[pos] or ".aux"	in	allfiles_[pos] or ".ps" in	allfiles_[pos] or ".log" in	allfiles_[pos] or  ".dvi" in	allfiles_[pos]:
			args=["rm", allfiles_[pos]];
			#print args;
			p=subprocess.Popen(args);
			p.communicate();

		if	".pdf"	in	allfiles_[pos]:
			args=["mv", allfiles_[pos], "../pdfs/"]
			p=subprocess.Popen(args);
			p.communicate();


sortfiles();
doActualMapping();	
generateTex();
generatePS();
generatePDF();
removeunncessaryFiles();
