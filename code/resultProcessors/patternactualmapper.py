import os;
import sys;

#param 1:  pdbid file
#param 2:  patternfile

class	Node:
	def	__init__(self, res_id, res_name,chain_name,xloc,yloc,fillcolor):
		self.residue_id_=res_id;
		self.residue_name_=res_name.strip();
		self.chain_name_=chain_name.strip();
		self.xloc=0.0;
		self.yloc=0.0;
		self.fillcolor="blue";

	def	__eq__(self, other):
		if	(self.residue_id_,self.residue_name_,self.chain_name_) == (other.residue_id_,other.residue_name_,other.chain_name_):
			return True;
		else:
			return False;

	def	__lt__(self,other):
		return self.residue_id_ < other.residue_id_;

	def	__gt__(self,other):
		return self.residue_id_ > other.residue_id_;

	def	__hash__(self):
		return hash ((self.residue_id_,self.residue_name_,self.chain_name_));

	def	getfillcolor(self):
		return self.fillcolor;

	def	setfillcolor(self,flc):
		self.fillcolor=flc;
	def	getxloc(self):
		return self.xloc;

	def	getyloc(self):
		return self.yloc;

	def	setxloc(self,locx):
		self.xloc=locx;

	def	setyloc(self,locy):
		self.yloc=locy;

	def	getchainname(self):
		return self.chain_name_;
	def	__str__(self):
		return str(self.residue_id_)+","+str(self.residue_name_)+str(",")+str(self.chain_name_);


class	EdgePair:
	def	__init__(self, node_1, node_2):
		self.node_1_= node_1;
		self.node_2_= node_2;

	def	__eq__(self, other):
		return ((self.node_1_,self.node_2_) == (other.node_1_,other.node_2_)) or ((self.node_2_, self.node_1_)== (other.node_1_, other.node_2_));
										
	      
	def	__lt__(self,other):
		return (self.node_1_,self.node_2_) < (other.node_1_, other.node_2_);

	def	__gt__(self, other):
		return (self.node_1_,self.node_2_) > (other.node_1_, other.node_2_);

	def	__hash__(self):
		return hash ((self.node_1_, self.node_2_));

	def	__str__(self):
		return str(self.node_1_)+","+str(self.node_2_);



def	getnodestr(nodeid):
	node_name = node_dict.get(int(nodeid));
	node_name_values =node_name.strip().split(",");
	node_str="";
	for	in_	in	range(0,len(node_name_values)):
		if	in_ > 0:
			node_str = node_str+"-"+str(node_name_values[in_]);
		else:
			node_str = node_str+str(node_name_values[in_]);
	
	return node_str;

def	getNode(nodeid):
	node_name = node_dict.get(int(nodeid));
	node_name_values =node_name.strip().split(",");
	n=Node (int(node_name_values[0]),node_name_values[1],node_name_values[2],0.0,0.0,"blue");	
	return n;
	
def	insertintochaincorrespondingnode(chain_corresponding_nodes,node_):
	if	node_.getchainname() not in	chain_corresponding_nodes.keys():
		node_list=[];
		node_list.append(node_);
		chain_corresponding_nodes[node_.getchainname()] = node_list;
	else:
		node_list=chain_corresponding_nodes.get(node_.getchainname());
		node_list.append(node_);
		chain_corresponding_nodes[node_.getchainname()] = node_list;
	


def	writeheader (texfiletowrite):
	texfiletowrite.write ("\documentclass[10pt]{article}"+os.linesep);
	texfiletowrite.write ("\pagestyle {empty}"+os.linesep);
	texfiletowrite.write ("\usepackage {pstricks}"+os.linesep);
	texfiletowrite.write ("\usepackage {pst-node}"+os.linesep);
	texfiletowrite.write ("\usepackage {pst-plot}"+os.linesep);
	texfiletowrite.write ("\\begin {document}"+os.linesep);

def	writefooter (texfiletowrite):
	texfiletowrite.write("\end{document}");

pdb_id_file=open(sys.argv[1]);

pdblist=[];
for	line	in	pdb_id_file:
	line =line.strip();
	pdbs =line.split(",");
	for	pos	in	range(0,len(pdbs)):
		if	len(str(pdbs[pos].strip()))== 4:
			pdblist.append(pdbs[pos]);



pattern_file=open(sys.argv[2]);
pattern_dir ="../../../nodemappings/"
filetowrite = open("../actual/"+sys.argv[2][:sys.argv[2].find(".")]+"_actual.txt","w");
texfiletowrite=open ("../tex/"+sys.argv[2]+"_actual.tex","w");
writeheader(texfiletowrite);
all_filetowrite = open(sys.argv[3],"a");

pattfreq=0;
pattcount=0;

for	line	in pattern_file:
	if	"Patt"	in	line:
		if	pattcount==0:
			pattcount=pattcount+1;
		else:
		        pattfreq=int(line.strip()[line.strip().find("[")+1: line.strip().rfind("]")]);	
	
	if	":" 	in	line:
		edge_dict={};
		id_edge_seq = line.split(":");
		graph_id =int(id_edge_seq[0]);
		node_mapping_file = open(pattern_dir+str(graph_id)+"_"+str(pdblist[graph_id-1])+"_nodes_mapping.txt");
		#print node_mapping_file;	
		node_dict={};
		for	mapping_line	in	node_mapping_file:
			mapping_line_elems= mapping_line.split(":");
			node_dict[int(mapping_line_elems[0])] = mapping_line_elems[1].strip();

		edge_seq=id_edge_seq[1].strip();

		edge_seq_list =edge_seq.split(")");
		texfiletowrite.write ("\\newpage"+os.linesep);
		filetowrite.write("["+str(graph_id)+"]:"+"["+str(pdblist[graph_id-1])+"]"+":[");
		all_filetowrite.write("[ Graph ID: "+str(graph_id)+"]:"+"[ Pattern Freq: "+str(pattfreq)+"]"+"[ Graph Name: "+str(pdblist[graph_id-1])+"]"+os.linesep);

		texfiletowrite.write("[ Pattern Freq: "+str(pattfreq)+"]"+ "[ Graph ID: "+str(graph_id)+" ] : "+"[ PDB  Name: "+str(pdblist[graph_id-1])+" ]"+" [ Edge List:  ");
		#print edge_seq_list;
		for	pos	in	range(0,len(edge_seq_list)):
			if	len(edge_seq_list[pos])<1:
				continue;
			else:
				#print edge_seq_list[pos];
				#second vertex of an edge
				nodes_val=edge_seq_list[pos].split(",");
				secondvertex= getnodestr(int(nodes_val[1]));
				n2= getNode (int(nodes_val[1]));

				nodes_val_seq=nodes_val[0].split("(");
				firstvertex= getnodestr (int(nodes_val_seq[1]));
				n1= getNode (int(nodes_val_seq[1]));
				e=EdgePair (n1,n2); # no edge duplication
				edge_dict [e] =1;
				filetowrite.write("("+firstvertex+","+secondvertex+")");
				texfiletowrite.write("("+firstvertex+","+secondvertex+")");
	
		filetowrite.write("]"+os.linesep);
		texfiletowrite.write(" ]"+os.linesep);
		texfiletowrite.write("\\\\"+os.linesep);
		texfiletowrite.write("\\\\"+os.linesep);
		texfiletowrite.write("\\\\"+os.linesep);

		texfiletowrite.write("\\begin{pspicture}(13,8)"+os.linesep);
		texfiletowrite.write("\psframe(0,-5) (13,8)"+os.linesep);
		# set location of nodes
		chain_corresponding_nodes={};
		chain_offset={};

		for	k	in	sorted(edge_dict.keys()):
			insertintochaincorrespondingnode (chain_corresponding_nodes,k.node_1_);
			insertintochaincorrespondingnode (chain_corresponding_nodes,k.node_2_);
		
		chain_count=0;
		node_dictionary={};

		for	k,v	in	sorted(chain_corresponding_nodes.items()):
			# print str(k)+" "+str(len(set(v)));
			set_of_verts= list(set(sorted(v)));
			# set the location
			offset = float(10.0 /float(len(set_of_verts)));
			chain_offset[k]=offset;

			filetowrite.write ("Chain "+str(k)+"=> residue: ");
			all_filetowrite.write ("Chain "+str(k)+"=> residue: ");

			topy =6.0;
			for	pos	in	range(0,len(set_of_verts)):
				if	pos == len(set_of_verts) -1:
					filetowrite.write (str(set_of_verts[pos].residue_id_));
					all_filetowrite.write (str(set_of_verts[pos].residue_id_));

				else:
					filetowrite.write (str(set_of_verts[pos].residue_id_)+" + ");
					all_filetowrite.write (str(set_of_verts[pos].residue_id_)+" + ");


				if	chain_count==0:
					set_of_verts[pos].setxloc(3);
				else:
					set_of_verts[pos].setxloc(10);
					set_of_verts[pos].setfillcolor ("red");

				set_of_verts[pos].setyloc(topy);

				topy= round(topy - offset,4);
				#print topy;
				node_dictionary[set_of_verts[pos]]=set_of_verts[pos];
			filetowrite.write (os.linesep);
			all_filetowrite.write (os.linesep);

			chain_count=chain_count +1;	

		minoffset=20.0;

		for	k	in	sorted(chain_offset.keys()):
			if	minoffset>float(chain_offset[k]):
				minoffset=float(chain_offset[k]);

		#print "----";
		#print minoffset;
		# put cirlces
		for	k	in	node_dictionary.keys():
			#print str(k.getxloc());
			#print str(k.getyloc());

			texfiletowrite.write("\\rput "+"("+str(k.getxloc())+","+str(k.getyloc()+(minoffset/2.0))+")"+"{"+str(k)+"}");
			texfiletowrite.write("\pscircle [ fillstyle=solid,fillcolor="+str(k.getfillcolor())+"]"+"("+str(k.getxloc())+","+str(k.getyloc())+")"+"{0.2}"+os.linesep);
		for	k	in	sorted(edge_dict.keys()):
			if	k.node_1_.chain_name_ == k.node_2_.chain_name_:
				texfiletowrite.write("\psline [linecolor=green,linewidth=2pt,linearc=0.25] {-}");
				texfiletowrite.write("("+str(node_dictionary[k.node_1_].getxloc())+","+str(node_dictionary[k.node_1_].getyloc())+")");	
				texfiletowrite.write("("+str(node_dictionary[k.node_1_].getxloc()+1.0)+","+str(node_dictionary[k.node_1_].getyloc())+")");	
				texfiletowrite.write("("+str(node_dictionary[k.node_2_].getxloc())+","+str(node_dictionary[k.node_2_].getyloc())+")");	
	
			else:	
				texfiletowrite.write("\psline [linewidth=2pt] {-}");
				texfiletowrite.write("("+str(node_dictionary[k.node_1_].getxloc())+","+str(node_dictionary[k.node_1_].getyloc())+")");	
				texfiletowrite.write("("+str(node_dictionary[k.node_2_].getxloc())+","+str(node_dictionary[k.node_2_].getyloc())+")");	
			texfiletowrite.write(os.linesep);

		chain_corresponding_nodes.clear();
				
		texfiletowrite.write("\\end{pspicture}"+os.linesep);
		
	
writefooter (texfiletowrite);
all_filetowrite.close();
