import os;
import sys;


interested_residue_id={};



def	readresidue_id_file ():
	interested_residue_id_file = open (sys.argv[1]);

	for	line	in	interested_residue_id_file:
		interested_residue_id[int(line.strip())] = 1;


def	read_all_pattern_residue_file_and_list_pattern_number(statfile):

	global interested_residue_id;
	all_pattern_residues_file = open(sys.argv[2]);

	pattern_id ="";

	maxmatch=0;
	matchcount=0;
	inflag=False;
	newpatternflag=False;

	for	line	in	all_pattern_residues_file:
		if	"No." in	line:
			if	newpatternflag==True:
				statfile.write (pattern_id_text+"#"+str(maxmatch)+"#"+os.linesep);
			pattern_id_text=line;
			maxmatch=0;
			newpatternflag=True;

	
		if	"Graph"	in	line:
			if 	inflag==True:
				if	maxmatch <matchcount:
					maxmatch=matchcount;	
			matchcount=0;
			inflag=True;

		if	"Chain"	in	line:
			residue_id_string = line[line.find(":")+1:];
			residue_ids = residue_id_string.strip().split("+");
	
			for	pos	in	range(0,len(residue_ids)):
				residue_id=residue_ids[pos].strip();
				residue_="";
				if	len(residue_id)>2:
					#get last two digit
					residue_=residue_id[-2:];
				else:
					residue_=residue_id;

				if	int(residue_) in	interested_residue_id:
					matchcount=matchcount+1;	

			

readresidue_id_file ();
statfile=open("matchstat.txt","w");
read_all_pattern_residue_file_and_list_pattern_number(statfile);
