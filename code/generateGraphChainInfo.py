import os;
import sys;

# @param1: pdb id file

pdbidfile =open (sys.argv[1]);

pdblist=[];

for	line	in	pdbidfile:
	lineelems=line.strip().split (",");
	for	pos	in	range(0,len(lineelems)):
		if	len(lineelems[pos]) >1:
			pdblist.append (lineelems[pos]);

filetowrite =open("graph_chain_info.txt","w");

for	pos_	in	range (0,len(pdblist)):
	filetoopen = open ( "nodemappings/"+str(pos_+1)+"_"+str(pdblist[pos_])+"_"+"nodes_mapping.txt");
	chain_residue_id_dict ={};

	for	line	in	filetoopen:
		lineelems =line.strip().split (":");

		residueinfo = lineelems [1].strip().split(",");

		if	str(residueinfo [2])	in	chain_residue_id_dict:
			residue_list = chain_residue_id_dict.get (residueinfo[2]);	
			residue_list.append(int(residueinfo[0]));
		else:
			chain_residue_id_dict [str(residueinfo[2])] = [int(residueinfo[0])];


	filetowrite.write ("-----Graph Name---- "+ str(pdblist[pos_])+" -----------"+os.linesep);

	for	k,v	in	chain_residue_id_dict.items():	
		
		vallist = list (set (v));
		print len(vallist);
		filetowrite.write ("Chain Name =>"+str(k)+" residue:");
		for	inpos_	in	range (0,len(vallist)):
			if	inpos_ == len(vallist)-1:	
				filetowrite.write (str(vallist[inpos_]) );		
			else:
				filetowrite.write (str(vallist[inpos_]) +" + ");		

		filetowrite.write (os.linesep);
