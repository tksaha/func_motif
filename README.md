# Discovery of Functional Motifs from the Interface Region of Oligomeric Proteins using Frequent Subgraph Mining Method
Studying the interface region of a protein complex paves the way for
understanding its dynamics and functionalities. Existing works study a protein
interface region by the composition of its residues, by the geometry of the
interface residues, or by directly aligning interface regions.  Very few works
use graphs as the tool for modeling the interface regions. In this work, we use
interface residues for forming networks from a set of protein structures, and
then find subgraphs that are frequent in those networks. For finding such
subgraphs, we use a scalable frequent subgraph mining algorithm, which can mine
frequent sub-network patterns of a specific size. We then discover the
functional motif along the interface region of a given protein from those mined
subgraphs.

## Requirements
* [Anaconda with Python 3.5](https://www.continuum.io/downloads)

## Python Environment setup and Update
* Run the following command:

```
conda env create -f func_motif.yml
```

Now, you have successfully installed sen2vec environment and now you can activate the environment using the following command. 

```
source activate func_motif
```

If you have added more packages into the environment, you 
can update the .yml file using the following command: 

```
conda env export > func_motif.yml
```


# Citation

If you are using the code, please consider citing the following work:

```
@article{saha2017discovery,
  title={Discovery of Functional Motifs from the Interface Region of Oligomeric Proteins using Frequent Subgraph Mining},
  author={Saha, Tanay Kumar and Katebi, Ataur and Dhifli, Wajdi and Al Hasan, Mohammad},
  journal={IEEE/ACM transactions on computational biology and bioinformatics},
  year={2017},
  publisher={IEEE}
}

```