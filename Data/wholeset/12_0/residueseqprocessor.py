import os;
import sys;


# Param 1: residue File (edgeseq File)
# Param 2: pattern File

# edgeseq file
residuefile=open(sys.argv[1]);
residue_dict={};

for line    in  residuefile:
    if  ":"     in line:
        linevalues=line.strip().split(":");
        #print len(linevalues);
        if  linevalues[1].strip()   not in  residue_dict:
            residue_id_list= [];
            residue_id_list.append(linevalues[0].strip()+":"+linevalues[2].strip());
            residue_dict[linevalues[1].strip()]= residue_id_list;

        else:
            residue_id_list= residue_dict.get(linevalues[1].strip())
            residue_id_list.append(linevalues[0].strip()+":"+linevalues[2].strip());
            residue_dict[linevalues[1].strip()]= residue_id_list;


# top pattern file
patternfile=open(sys.argv[2]);
patterns=[];

count=1;
#print len(residue_dict);

for line in     patternfile:
    
    pattern=line.strip()[ line.strip().find("#")+1: line.strip().rfind("#")];
    #print pattern;
    if  pattern.strip() not in  residue_dict:
        continue;
    residue_id_list = residue_dict.get(pattern.strip());

    #print len(residue_id_list);
    freq= line.strip() [line.strip().rfind("#") +1 : line.strip().rfind("|")];

    filetowrite = open("pattern/pattern_"+str(count)+"_residue_id_file.txt","w");
    filetowrite.write ("Pattern Canonical Code [ "+pattern+"]"+os.linesep);
    filetowrite.write ("Pattern Freq ["+str (freq)+"]"+os.linesep);

    id_dictionary={};
    for pos in  range(0,len(residue_id_list)):
        id_seq= residue_id_list[pos];
        id_seq_vals=id_seq.split(":");
        

        if  int(id_seq_vals[0]) not in  id_dictionary:
            seq_list=[];
            seq_list.append(id_seq_vals[1]);
            id_dictionary[int(id_seq_vals[0])]=seq_list;

        else:
            seq_list=id_dictionary.get(int(id_seq_vals[0]));
            seq_list.append(id_seq_vals[1]);
            id_dictionary[int(id_seq_vals[0])]=seq_list;
    
        
    for k   in  sorted(id_dictionary.keys()):
        #filetowrite.write("-------------"+str(k)+"------------"+os.linesep);
        seq_list=id_dictionary[k];
        #print len(seq_list);
        seq_list=list(set(seq_list));

        for pos in  range(0,len(seq_list)):
            filetowrite.write(str(k)+":"+seq_list[pos]+os.linesep);

    filetowrite.write("Total pdbs here->"+str(len(id_dictionary))+os.linesep);  

    count=count+1;
