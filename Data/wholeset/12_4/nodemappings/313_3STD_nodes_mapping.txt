0:2,GLY,A
1:10,GLU,A
2:11,ILE,A
3:12,THR,B
4:12,THR,A
5:13,PHE,B
6:13,PHE,A
7:14,SER,A
8:14,SER,B
9:15,ASP,B
10:15,ASP,A
11:16,TYR,A
12:16,TYR,B
13:17,LEU,A
14:17,LEU,B
15:18,GLY,B
16:19,LEU,B
17:20,MET,B
18:21,THR,B
19:22,CYS,B
20:23,VAL,B
21:24,TYR,B
22:25,GLU,B
23:26,TRP,B
24:27,ALA,B
25:28,ASP,B
26:29,SER,B
27:31,ASP,B
28:32,SER,B
29:33,LYS,B
30:80,THR,B
31:81,LEU,B
32:82,ARG,B
33:83,THR,B
34:84,GLN,B
35:85,HIS,B
36:86,PHE,B
37:86,PHE,A
38:87,ILE,A
39:87,ILE,B
40:88,GLY,B
41:88,GLY,A
42:89,GLY,A
43:89,GLY,B
44:90,THR,B
45:90,THR,A
46:91,ARG,A
47:92,TRP,A
48:93,GLU,A
49:94,LYS,A
50:102,GLY,A
51:103,TYR,A
52:104,HIS,A
53:104,HIS,B
54:105,GLN,B
55:105,GLN,A
56:106,LEU,B
57:106,LEU,A
58:107,ARG,A
59:107,ARG,B
60:108,VAL,A
61:108,VAL,B
62:109,PRO,B
63:110,HIS,B
64:111,GLN,B
65:112,ARG,B
66:113,TYR,B
67:118,MET,B
68:119,LYS,B
69:120,GLU,B
70:121,VAL,B
71:123,MET,B
72:124,LYS,B
73:125,GLY,B
74:126,HIS,B
75:126,HIS,A
76:127,ALA,A
77:128,HIS,A
78:129,SER,A
79:130,ALA,A
80:131,ASN,A
81:150,ASP,A
82:151,ILE,A
83:152,ARG,A
84:153,TRP,A
85:154,GLY,A
86:155,GLU,A
87:156,PHE,A
88:157,ASP,A
89:159,ASP,A
