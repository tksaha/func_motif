import os 
import sys 


file_to_read = open(sys.argv[1]) # all pattern residue file
nodemapping_dir = sys.argv[2] # node mapping directory

# Create a directory called all_Data

graph_id = 0 
graph_name = ""
chain_a_residue_dict = {}
chain_b_residue_dict = {}

graph_name_dict = {}

file_to_write = ""
str_ids =""
str_names =""
str_list =[]
residue_name_dict = {}


for line in file_to_read:
    if "Pattern No." in line:
        #print (line.strip())
        pass 
    else:
        if "Graph" in line:
            str_ids = ""
            str_names = ""
            str_list = []
            graph_id = line.strip()[line.strip().find("ID:")+3: \
                line.strip().find("]")].strip()

            graph_name = line.strip()[line.strip().find("Name:")+5: \
                line.strip().rfind("]")].strip()

            # read node dictionary again
            # chain + residue id as key
            # value is amino acid name 
            file_to_read = open(os.path.join(nodemapping_dir,"%s_%s_nodes_mapping.txt"%(graph_id, graph_name)))
            chain_a_residue_dict.clear()
            chain_b_residue_dict.clear()

            for line_in in file_to_read:
                line_elems = line_in.strip().split(":")[1].split(",")
                id_ = int(line_elems[0])
                name = line_elems[1]

                if name not in graph_name_dict:
                    graph_name_dict[name] =1 
                file_to_write = open(os.path.join("all_Data","%s.txt"%graph_name),"a")



                chain_ = line_elems[2].strip()

                if chain_=="A":
                    chain_a_residue_dict[id_] = name;
                else:
                    chain_b_residue_dict[id_] = name;

            str_list.append("%s%s"%(line.strip(),os.linesep))
            #file_to_write.write("%s%s"%(line.strip(),os.linesep))
            #print (line.strip())
    
            
        #signal = 'NO'       
        
            
        if "Chain A" in line: 
            #file_to_write.write("%s%s"%(line.strip(),os.linesep))
            #print (line.strip())
            residue_name_dict = {}
            str_ids = "%s%s"%(line.strip(),os.linesep);
            str_names = "Chain A=> residue_names:";
            all_ids = line[ line.find(":")+1: ]
            line_elems = all_ids.split("+")

            for pos in range (0, len(line_elems)):
                if pos == len(line_elems)-1:
                    str_names = "%s%s "%(str_names,chain_a_residue_dict[int(line_elems[pos])])
                    residue_name_dict[chain_a_residue_dict[int(line_elems[pos])]]= 1    
                else:
                    str_names = "%s%s + "%(str_names,chain_a_residue_dict[int(line_elems[pos])])
                    residue_name_dict[chain_a_residue_dict[int(line_elems[pos])]]= 1    
    
    
            #str_list = list()   
            str_list.append(str_ids)
            str_list.append(str_names)  
            #file_to_write.write("%s"%(str_ids))
            #file_to_write.write("%s%s"%(str_names, os.linesep))
            #print (str_)
        if "Chain B" in line: 
            #file_to_write.write("%s%s"%(line.strip(),os.linesep))
            #print (line.strip())
            str_ids =  "%s%s"%(line.strip(), os.linesep)
            str_names = "Chain B=> residue_names:"
            all_ids = line[ line.find(":")+1: ]
            line_elems = all_ids.split("+")

            for pos in range (0, len(line_elems)):
                if pos == len(line_elems)-1:
                    str_names = "%s%s "%(str_names,chain_b_residue_dict[int(line_elems[pos])])
                    residue_name_dict[chain_b_residue_dict[int(line_elems[pos])]]= 1    

                else:
                    str_names = "%s%s + "%(str_names,chain_b_residue_dict[int(line_elems[pos])])
                    residue_name_dict[chain_b_residue_dict[int(line_elems[pos])]]= 1    
    
            #file_to_write.write ("%s"%(str_ids))
            #file_to_write.write ("%s%s"%(str_names, os.linesep))
            str_list.append(str_ids)
            str_list.append(str_names)
            #print (len(str_list))
            
            #file_to_write.write("%s"%(str_ids))
            #file_to_write.write("%s%s"%(str_names, os.linesep))
            #file_to_write.write("%s%s"%(str_,os.linesep))
            #print (str_)

            filter_ = int(sys.argv[3])

            if len(str_list) < 5: 
                continue 
            if filter_==1:
                if 'ASN' in residue_name_dict and 'GLU' in residue_name_dict and 'SER' in residue_name_dict:
                    file_to_write.write("%s"%(str_list[0]))
                    file_to_write.write("%s"%(str_list[1]))
                    file_to_write.write("%s%s"%(str_list[2], os.linesep))
                    file_to_write.write("%s"%(str_list[3]))
                    file_to_write.write("%s%s"%(str_list[4], os.linesep))
            if filter_ ==2:
                if 'ASN' in residue_name_dict or 'GLU' in residue_name_dict or 'SER' in residue_name_dict:
                    file_to_write.write("%s"%(str_list[0]))
                    file_to_write.write("%s"%(str_list[1]))
                    file_to_write.write("%s%s"%(str_list[2], os.linesep))
                    file_to_write.write("%s"%(str_list[3]))
                    file_to_write.write("%s%s"%(str_list[4], os.linesep))
            else:
                file_to_write.write("%s"%(str_list[0]))
                file_to_write.write("%s"%(str_list[1]))
                file_to_write.write("%s%s"%(str_list[2], os.linesep))
                file_to_write.write("%s"%(str_list[3]))
                file_to_write.write("%s%s"%(str_list[4], os.linesep))