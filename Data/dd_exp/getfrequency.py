import os 
import sys 
import networkx as nx 
import networkx.algorithms.isomorphism as iso

group_id = int(sys.argv[1])

graph_list = []


label_dict = {}
label_file = os.path.join("exp_group_%i/labelmappings/label_mapping.txt"%group_id)
for line in open(label_file):
	line_elements = line.strip().split(":")
	if len(line_elements) < 2:
		break
	label_dict[int(line_elements[0])] = line_elements[1]



pos_list = list(range(5,7))
for patternsize in range(5,7):

	pattern_file_to_read = 	os.path.join("exp_group_%i"%group_id,\
		 "toppatterns_%i_%i.txt"%(patternsize, group_id))
	graphs = []
	for line in open(pattern_file_to_read):
		g = nx.Graph()
		line=line.strip();
		edgeseq=line[line.find ("#")+1: line.rfind ("#")];
		patternElems_  = edgeseq.strip().split (")");
		for	patterns_	in	patternElems_:
			if	len(patterns_) ==0:
				continue;
			
			edgeelements_ = patterns_.split(",")
			firstnode= edgeelements_[0].split("(")
			st = int (firstnode[1])
			end = int (edgeelements_[1])
			stl =label_dict[int (edgeelements_ [2])]
			edgel=int (edgeelements_ [3])
			endl =label_dict[int (edgeelements_ [4])]
			g.add_node(st, label=stl)
			g.add_node(end, label=endl)
			g.add_edge(st,end)
		graphs.append(g)

	graph_list.append(graphs)

nm = iso.categorical_node_match('label',1)
for pos in range(0, len(graph_list)):
	graphs = graph_list[pos]	
	for inpos in range(pos+1, len(graph_list)):
		graphs_in = graph_list[inpos]
		
		index = 0
		pattsize = pos_list[pos]

		for g in graphs:
			count = 0
			index +=1
			for g1  in graphs_in:
				instance=iso.GraphMatcher(g1,g, nm)
				if instance.subgraph_is_isomorphic():
					count = count + 1
			print ("%i,%i,%i,%i"%(index, pos_list[pos],pos_list[inpos],count))
