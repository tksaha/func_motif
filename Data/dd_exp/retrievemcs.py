import networkx as nx 
import os 
import sys
from itertools import chain, combinations
import networkx as nx 
import networkx.algorithms.isomorphism as iso

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def all_subgraphs(graph):
   for vertices in powerset(range(graph.number_of_nodes())):
   	   	if len(vertices) < 2: 
   	   		continue
   	   	yield graph.subgraph(vertices)


group_id = int(sys.argv[1])
graph_list = []


label_dict = {}
reverse_label_dict = {}
label_file = os.path.join("exp_group_%i/labelmappings/label_mapping.txt"%group_id)
for line in open(label_file):
	line_elements = line.strip().split(":")
	if len(line_elements) < 2:
		break
	label_dict[int(line_elements[0])] = line_elements[1]
	reverse_label_dict[line_elements[1]] = int(line_elements[0])



pos_list = list(range(8,9))
for patternsize in range(8,9):

	pattern_file_to_read = 	os.path.join("exp_group_%i"%group_id,\
		 "toppatterns_%i_%i.txt"%(patternsize, group_id))
	graphs = []
	for line in open(pattern_file_to_read):
		g = nx.Graph()
		line=line.strip();
		edgeseq=line[line.find ("#")+1: line.rfind ("#")];
		patternElems_  = edgeseq.strip().split (")");
		for	patterns_	in	patternElems_:
			if	len(patterns_) ==0:
				continue;
			
			edgeelements_ = patterns_.split(",")
			firstnode= edgeelements_[0].split("(")
			st = int (firstnode[1])
			end = int (edgeelements_[1])
			stl =label_dict[int (edgeelements_ [2])]
			edgel=int (edgeelements_ [3])
			endl =label_dict[int (edgeelements_ [4])]
			g.add_node(st, label=stl)
			g.add_node(end, label=endl)
			g.add_edge(st,end)
		graphs.append(g)

	graph_list.append(graphs)

nm = iso.categorical_node_match('label',1)
g0 = graph_list[0]
g1 = graph_list[0]



size_list = [3,4]
freq_list = [69, 65]

index = 0

for g in g0:
	index +=1 
	
	for subgraph_ in all_subgraphs(g):
		if subgraph_.number_of_nodes() not in size_list:
			continue;
		if nx.is_connected(subgraph_) == False:
			continue
		count = 0
		for gx in g1:
			instance=iso.GraphMatcher(gx,subgraph_, nm)
			if instance.subgraph_is_isomorphic():
				count = count+1
		
		len_ = len(size_list)

		for pos in range(0,len_):
			if  subgraph_.number_of_nodes()==size_list[pos] \
				and count == freq_list[pos]:
				# print the pattern
				#print (nx.is_connected(subgraph_))

				pattern ="#"
				for edges in subgraph_.edges():

					#print (subgraph_.node[edges[0]]['label'])
					#print (subgraph_.node[edges[1]]['label'])


					pattern = "%s(%s,%s,%s,%s,%s)"%(pattern, edges[0], edges[1],\
							reverse_label_dict[subgraph_.node[edges[0]]['label']],"1",\
							reverse_label_dict[subgraph_.node[edges[1]]['label']])

				pattern = "%s#"%pattern
				print (pattern)

				#print ("%i,%i,%i" %(index,subgraph_.number_of_nodes(), count))
		

