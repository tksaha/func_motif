import os;
import sys;


vertexcountdict={};

		
def	writevertex(acgmfiletowrite,graphcount,st,stl):
	acgmfiletowrite.write("node "+str(graphcount)+" "+str(int(st)+1)+" "+str(stl)+os.linesep);


def	writeedge(acgmfiletowrite,graphcount,st,end,edgel):
	acgmfiletowrite.write("edge "+str(graphcount)+" "+str(int(st)+1)+" "+str(int(end)+1)+" "+str(edgel)+os.linesep);


def	parseGraph(graphfile,spinnodefile,spinedgefile):	
	graphcount=-1;
	for	line in	graphfile:
		if	"t"	in	line:
			graphcount=graphcount+1;

		elif 	"v"	in	line:
		    	vertexelems=line.strip().split(" ");
			writevertex(spinnodefile,graphcount,str(vertexelems[1]),str(vertexelems[2]));
		
		elif	"e"	in	line:
			edgeelems =line.strip().split(" ");
			writeedge(spinedgefile,graphcount,str(edgeelems[1]),str(edgeelems[2]),str(edgeelems[3]));	



def	formatspin():
	graphfile=open(sys.argv[1]);
	spinnodefile=open(sys.argv[1]+"-spinN","w");
	spinedgefile=open(sys.argv[1]+"-spinE","w");
	graphfile=open(sys.argv[1]);
	parseGraph(graphfile,spinnodefile,spinedgefile);	
		

formatspin();
