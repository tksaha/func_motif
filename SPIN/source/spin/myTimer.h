#ifndef  MYTIMER
#define  MYTIMER
#include <map>
#include <string>
#include "common.h"

#ifdef LINUX
	#include <sys/time.h>
	#include <unistd.h> 
#else
	#include <windows.h>
#endif

using namespace std;

class MyTimer
{
private:
	vector<double> elapsed;
	vector<double> start;
	map<string, int> timers;
	int size;

#ifdef LINUX
	struct timeval zero;
	struct timezone tz; 
#else 
	double zero;
#endif

	double get_currentTime();
	int    get_index(string  s);

public:
	MyTimer(){

#ifdef LINUX
	int i = gettimeofday(&zero, &tz);
#else 
	 zero = GetTickCount();
#endif	
	 size = 0;

	}

	void clear_timer(string  s);
	void allocate_timer(string  s);
	void start_timer(string  s);
	void stop_timer(string s);
	double  value_timer(string  s);
	void print();
};

#endif
