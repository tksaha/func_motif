/**************************************************************************
 * adj_matrix.h
 *
 **************************************************************************/

#ifndef ADJ_MATRIX_H
#define ADJ_MATRIX_H

#include <vector>
#include <set>
#include <string>
#include "common.h"
#include "cost.h"
#include "occur.h"
#if defined (VFLIB)
	#include "argraph.h"
	#include "argedit.h"
	#include "attribute.h"
#endif

#define HALFKEY 16
#define aNULLKEY 0x00007fff
#define aMAKEKEY(x, y) ( ( (x) << HALFKEY ) |  (y)  )
#define aFIRSTKEY(x)   ( ( (x) >> HALFKEY ) )
#define aSECONDKEY(x)  ( ( (x)  << HALFKEY   >>  HALFKEY  ) )

typedef int KEYTYPE ;

class AdjMatrix 
{
private:
	vector<GLTYPE> matrix;
	//int ocsize;
	vector<vector<GNSIZE>*> SAdjList;
	int matrixSize;
	int eSize;

#ifdef SAVERESULTS
    map<int, int>  graphs;
	vector<occur*> occs;
	void print(int i, string f1, string f2, vector<int> & g_reindex);
	vector<int>    TIDs;
	int            totalSup;
	vector<GNSIZE> graphIDs;
#endif

#if defined VFLIB
	ARGraph<Point, void> * myg;
#endif

	void getMaxNode( vector<int> &v);
	void getMaxNode2( vector<int> &v);
	void last2bitil(int & i, int & l);

	inline void assignLabel(int i, int j, GLTYPE label){

#ifdef SAFEMODE
		int s = size();
		if( i < 0 || i >= s || j < 0 || j >=s ){
			cout << "i is " << i << " j is " <<  j << endl;
			error("index outof boundary");
		}
#endif
		if( i < j) swap(i, j);
		matrix[i*(i+1)/2+j] = label;
	}

	inline void pushLabel(GLTYPE label){
		matrix.push_back(label);
	}

	int  isTreeBasedCanonicalForm();
	int  isTBRecCanonicalForm( vector<GNSIZE> & occ, vector<int> & ref , vector<int> & ref2, int pos1, int pos2);
	void getCanonicalForm(vector<GNSIZE> & occ, vector<int> & ref);
	void buildAdjList();

public:

	AdjMatrix(){ 
		//ocsize = 0; 
		//SAdjList = NULL; 
		matrixSize=0;
		eSize = 0;
#ifdef VFLIB
  	    myg = NULL;
#endif
	}

	//constructor
	AdjMatrix(AdjMatrix * M, int i, int j, GLTYPE el, GLTYPE nl){

		matrixSize = M->matrixSize;
		matrix = M->matrix;
		eSize = M->eSize;
		buildAdjList();
		
		if( i >=  matrixSize ) addNode(nl);
		addEdge(i, j, el);
		
#ifdef  VFLIB 
		myg = NULL;
#endif
	}

	//constructor
	AdjMatrix(AdjMatrix * M, Cost & lc){

		int s = M->size();
		matrixSize = M->matrixSize;
		eSize = M->eSize;
		//copy(M->matrix.begin(), M->matrix.end(), matrix.begin());
		matrix = M->matrix;
		for( s=0; s< lc.size(); s++)
			pushLabel(lc.node(s));

		//SAdjList = NULL;
        //buildAdjList(); 

#ifdef  VFLIB 
		myg = NULL;
#endif
	}

	//constructor
	AdjMatrix(const AdjMatrix * M){

		matrixSize = M->matrixSize;
		eSize = M->eSize;
		matrix = M->matrix;
		//SAdjList = NULL;
        buildAdjList(); 

#ifdef  VFLIB 
		myg = NULL;
#endif
	}

	~AdjMatrix() { 
	
		int i;
		for( i=0; i< SAdjList.size(); i++)  delete SAdjList[i];

#ifdef SAVERESULTS
		for(i=0; i< occs.size(); i++){
			delete occs[i];
		}
#endif

	}

	vector<GNSIZE> * getNeighbors(int index);
	void getNeighbors(int index, vector<GNSIZE> &v);

	int  isCanonicalForm1(); 
	int  isCanonicalForm2() ;
	int  isCanonicalForm();
	int  isTreeCanonicalForm();
	void getReferrence(vector<int> &ref);
	void getCanonicalForm(vector<int> & ref);
	void getBoundary(vector<int> & ref);

    bool isPath();
	bool isTree(){ return ( matrixSize == (esize()+1) ); }
	inline GLTYPE getLabel(int i, int j) const { 
#ifdef SAFEMODE
		if( i< 0 || i >= size() || j < 0 || j>= size()) {
			cout << "i " << i << " j " << j << endl;
			error("label access out of boundary");
		}
#endif
		if( i < j) swap(i,j);
		return matrix[i*(i+1)/2+j];
	}

	inline GLTYPE getLabelFast(int i, int j) const { 
#ifdef SAFEMODE
		if( i< 0 || i >= size() || j < 0 || j>= size()) {
			cout << "i " << i << " j " << j << endl;
			error("label access out of boundary");
		}
		if( i < j) error(" i and j is not right in getting labels");
#endif
		return matrix[i*(i+1)/2+j];
	}

	inline int  size() const { return matrixSize; }
	int  esize();
	void print();

	void addNode(char label);
	void addEdge(int i, int j, char label);
	void removeEdge(int i, int j);
	string canonicalForm();
//	void setOCsize(int s){ ocsize = s ; }
	//void buildAdjList();
	bool isSubgraphOf(AdjMatrix * m, int f);
	bool isIsomorphicOf(AdjMatrix *m){ return isSubgraphOf(m, 0) && m->isSubgraphOf(this,0) ; }

#ifdef SAVERESULTS
     void print(int i, string f1, string f2, string f3, vector<int> & g_reindex);
     void setGraphs(vector<occur*> & occs1);
	 void setTIDs(GRAPHS1 & g,  int tsize);
	 void setGraphIDs(vector<GNSIZE> & gid);
	 vector<GNSIZE> & getGraphIDs(){ return graphIDs; }
#endif

#ifdef VFLIB 
	void setGraphForMatch();
	ARGraph<Point, void> * getARGraph(){ return myg; }
	bool isSubgraph(AdjMatrix * m);
	bool isIsomorphic(AdjMatrix * m);
	bool static my_visitor(int n, node_id ni1[], node_id ni2[], void *usr_data);
	int  findAllIsomophisms(AdjMatrix * m);
#endif

};

#endif
