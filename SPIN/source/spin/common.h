#ifndef COMMON_H
#define COMMON_H

//OS type
#define LINUX 1

//induced or not induced
//#define INDUCEDS 1

//maximal or not
#define MAXIMALPATTERN 1

//validation related macros
//#define VALIDATION 1

//running in safemode
//#define SAFEMODE	1

//keep the search results
#define SAVERESULTS   1

//search the frequent subgraphs based on spanning tree, 1 for closed tree
//2 for normal tree
#define TREEBASEDSCAN 1

//using VF lib
//#define VFLIB 1

//basic set up
#define EMPTY_EDGE '0'
#define NODE_TOKEN "node"
#define EDGE_TOKEN "edge"
#define LOGFILE    "sym.log"

//debug flags
//debug 1 for carnoical form related debug information
//debug 2 for candidate proposing related debug
//debug 3 for flow related debug
//debug 4 for sub carnonical form related debug
//debug 5 for initialization related debug
//debug 6 for large dataset debugging
//debug 7 for detailed candidate proposing related debug
//debug 8 for outer candidate proposing related debug
//debug 9 ?
//debug 10 for filter debug
//debug 11 for basic level maximal debug
//debug 12 for level 2 maximal pattern debug
//debug 13 for tree based canonical form debug
//debug 14 for filling in edges debug
//debug 15 for detailed tree based canonical form debug
//debug 16 for frequent element candidates debug
//debug 17 for maximal inner pattern debug
//debug 18 for look ahead debug

//#define DEBUG1 1
//#define DEBUG2 1
//#define DEBUG3 1
//#define DEBUG4 1
//#define DEBUG5 1
//#define DEBUG6 1
//#define DEBUG7 1
//#define DEBUG8 1
//#define DEBUG9 1
//#define DEBUG10 1
//#define DEBUG11 1
//#define DEBUG12 1
//#define DEBUG13 1
//#define DEBUG14 1
//#define DEBUG15 1
//#define DEBUG16 1
//#define DEBUG17 1
//#define DEBUG18 1

//timer related macros
//#define TIMERFLAG 1 
#define TIMER1   "Overall"
#define TIMER2   "subgraph_testing"
#define TIMER3   "children_proposing"
#define TIMER4	 "handling_data_structure"
#define TIMER5   "inner_proposing"
#define TIMER6   "outer_proposing"
#define TIMER7   "isomorphism"
#define TIMER8   "scanFrequentElement"
#define TIMER9   "fillingEdges"
#define TIMER10  "addInstances"

//registers related macros
//#define REGISTERFLAG 1
#define REGISTER1   "total_tried_children_proposing"
#define REGISTER2   "total_children_instance_proposed"
#define REGISTER3   "total_children"
#define REGISTER4   "passed"
#define REGISTER5   "okay"
#define REGISTER6   "failed"
#define REGISTER7   "no_frequent"
#define REGISTER8   "extendedd"
#define REGISTER9   "ext_total_children"
#define REGISTER10   "ext_passed"
#define REGISTER11   "ext_okay"
#define REGISTER12   "extfailed"
#define REGISTER13   "ext_no_frequent"

//log files
//#define   PFILE		"sym.pattern"
//#define   GFILE     "sym.graph"
//#define   TRAINING  1

#include <iostream>
#include <fstream>
#include <set>
#include <map>
#ifdef LINUX
	#include <ext/hash_map>
	#include <ext/hash_set>
#endif
using namespace std;

#include "myTimer.h"
#include "register.h"

//common types
#ifdef LINUX
	typedef long long DLONG;
	//typedef int DLONG;
#else
	typedef _int64 DLONG;
	//typedef int DLONG;
#endif

#define  HALFDLONG 32
#define  QUADLONG  16 
#define  GNULLINDEX 0x0000ffff
#define  INULLINDEX 0x7fffffff
	
typedef vector<DLONG>  COCCS;
typedef int   BTYPE;
typedef unsigned char BYTE;

//typedef unsigned char  GNSIZE;
typedef short GNSIZE;
typedef char  GLTYPE;

//macros use Ullman's backtract
#define   UEDGE     '1'
#define   UNEDGE    '0'

typedef vector<GNSIZE>      GRAPHS1;
typedef map<DLONG, int>     TRA; 
typedef vector<int>         IIDTYPE;
typedef vector<IIDTYPE*>    FREELE;


#ifdef LINUX
#define ASSOCONTAINOR hash_map
#else
#define ASSOCONTAINOR map
#endif

typedef map<int, int>  CGOCC;
typedef map<int, IIDTYPE*>  ELEFRENQC;

//related macros
#define MAKEKEY(x, y, z) ( ( ( (DLONG)(x) ) << HALFDLONG ) |  ((DLONG)(y) << QUADLONG ) | ((DLONG)(z)) )
#define FIRSTKEY(x)   ( (int) ( (x)  >> HALFDLONG ) )
#define SECONDKEY(x)  ( (int) ( ((x)  << HALFDLONG  >>  HALFDLONG) >> QUADLONG ) )
#define THIRDKEY(x)   ( ((int) ((x)  << HALFDLONG  >>  HALFDLONG) ) & 0x0000ffff)
#define CANONICALELE(x, y, z, w) ( ((127-(x)) << 24) | ((127-(y)) << 16) | ((z) << 8) | (w) )   //used for canonical searching

typedef int COSTTYPE; 
#define COSTLENGTH  16
#define LMASK   0x000000ff
#define MAKECOST(x, y, z)  ( ( ((COSTTYPE) (x))  << COSTLENGTH ) | ( (y) << (COSTLENGTH>>1) ) | (z)  )
#define COSTINDEX(x)  ( (x)  >> COSTLENGTH ) 
#define COSTEL(x)     ( ((x) >> (COSTLENGTH>>1) ) & LMASK )
#define COSTNL(x)     ( (x) & LMASK )

inline void error(const char *p, const char *p2 = " ")
{
	cerr << p << ' ' << p2 << endl;
	exit(1);
}

inline void log(const char *p, const char *p2 = " ")
{

#ifdef LINUX
	ofstream outfile(LOGFILE, ios::app);
#else
	ofstream outfile(LOGFILE, ios_base::app);
#endif

	if( !outfile )
		error("log file is not ready for write: ", LOGFILE);

	outfile << p << ' ' << p2 << endl;
	outfile.close();

}

#endif
