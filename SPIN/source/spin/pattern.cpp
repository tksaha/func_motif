#pragma warning (disable:4786 4018 4267)
#include <vector>
#include <fstream>
#include <algorithm>
#include <set>
using namespace std;

#include "pattChild.h"

gBase    * pattern ::gb = NULL;
MyTimer  * pattern ::Timers = NULL;
Register * pattern ::Registers = NULL;

void pattern :: initPattern(gBase *gb1, MyTimer & t, Register & mr){
		gb = gb1; Timers = &t; Registers = &mr; 	
}

int pattern:: isCarnonicalForm(void) { 

	stage = 1;
	
#ifdef TIMERFLAG
	Timers->start_timer(TIMER4);
#endif

	fixM(); 

#ifdef TIMERFLAG
	Timers->stop_timer(TIMER4);
#endif
	

#ifdef TIMERFLAG
	Timers->start_timer(TIMER2);
#endif

	int flag = M->isCanonicalForm();  
	
#ifdef TIMERFLAG
	Timers->stop_timer(TIMER2);
#endif

	return flag;

}

void pattern:: sprint(int level)
{
	cout << "the adj matrix is" << endl;
	if( M )
		M->print();
	else
		cout <<"matrix is not initialized" <<endl;
	
	cout << "pattern optimal "  << optimal << " cost " << hex << mycost.intValue() << dec << endl;

	if( level & 2  ){
		cout << "the coccs structures are: size: " << coccs->size() << endl;
		
		COCCS :: iterator ip1;
		for(ip1 = coccs->begin(); ip1 != coccs->end(); ip1++){
			DLONG key = *ip1;
			cout << "index is " << (FIRSTKEY(key) ) << " node is " << 
				 (THIRDKEY(key)) << " graph " << (SECONDKEY(key)) << endl;
		}
	}

	if( level & 4  ){
		cout << "there are total: " << child.size() << " children in this pattern " << endl;
		
		vector<pattern *> :: iterator ip1 ;
		for(ip1 = child.begin(); ip1 != child.end(); ip1++){
			(*ip1)->sprint(5);
		}
	}
}

void pattern:: print()
{
	cout << "the adj matrix is" << endl;
	if( M )
		M->print();
	else
		cout <<"matrix is not initialized" <<endl;

	//int i;
	cout << "the index structure are" << endl;
	COCCS :: iterator ip1;

	for(ip1 = coccs->begin(); ip1 != coccs->end(); ip1++){
		DLONG key = *ip1;
		cout << "index is " <<  (int)(FIRSTKEY(key) ) << " node is " << (int) (THIRDKEY(key)) << endl;
	}

	cout << "cost is " << hex << mycost.intValue() << endl;
}

int pattern:: fsm1( vector<AdjMatrix * > & result, int freq, int level){

	CHLTYPE :: iterator ip;
	//cout << "start search total frequent node :" << child.size() << endl;

	vector<pattern*> wlist;
	int totalnum = 0, totalInside =0;

	for( ip = child.begin(); ip!=child.end(); ip++){	
		(*ip)->fsm2(result, freq, level+1, totalnum, totalInside);	
		//delete the edge;
		delete (*ip);
	}

	cout << "Total tree: " << totalnum << " Total inside: "<< totalInside << endl;
	return totalInside;

#ifdef DEBUG8
	edbLocal->print();
#endif
	
}


void pattern:: fsm2( vector<AdjMatrix * > & results, int freq, int level,
					int& totalnum, int & totalInside){

	
#ifdef DEBUG2
	cout << "search at level " << level << endl;
	sprint(3);	
#endif

	stage = 4;
	if( level >= 2){
		totalnum++;
		if( totalnum % 1000 == 0 ) cout <<".";
		if( totalnum % 10000 == 0 ) cout << totalnum  << "\n";

#ifdef  MAXIMALPATTERN
		if( !GETCLOSEDBIT(optimal) && child.size() == 0 && isMaximal() && isMaximal(freq, true) ){
			totalInside++;
		}
#endif

	}

	pattern * pi, *pj;
    bool maximal;

	for( int i = 0; i< child.size(); i++){
		pi = child[i];
		vector<pattern*> outChild;

#ifdef DEBUG3
	    for( int lii = 0; lii< level; lii++)
			cout << "  ";
		cout << "cost and child size " << hex << pi->mycost.intValue() << dec << "  " <<  pi->child.size() << endl;
#endif

		if( (pi->optimal & 1) ){  //the pattern must be in carnonical form for subsequent search

			maximal = true;

			if( size != pi->size ) {	//only one edge in the last row

#ifdef TIMERFLAG
	            Timers->start_timer(TIMER3);
#endif
#ifdef TIMERFLAG
		        Timers->start_timer(TIMER6);
#endif
				maximal = pi->proposeOuterChild(freq, outChild);
				
#ifdef TIMERFLAG
		       Timers->stop_timer(TIMER6);
#endif
#ifdef TIMERFLAG
	          Timers->stop_timer(TIMER3);
#endif
			}

#ifdef MAXIMALPATTERN
			if( maximal ){
#endif

#ifdef DEBUG2
				cout << "children are (proposed by outer): " << endl;
				int k = 0;
				for( ; k< pi->outChild.size(); k++)	(pi->child[k])->sprint();
#endif

				for(int j = i; j< child.size(); j++){
					pj = child[j];
					pi->join(pj, freq);
				}

#ifdef DEBUG2
				cout << "completed child proposing" << endl;
				cout << "children are: " << endl;
				for( k=0; k< pi->child.size(); k++)		(pi->child[k])->sprint();			
#endif
				//post processing
				(pi->child).insert((pi->child).end(), (pi->newchild).begin(), (pi->newchild).end());
				(pi->child).insert((pi->child).end(), outChild.begin(), outChild.end());
				pi->fsm2( results,freq, level+1 , totalnum, totalInside);

#ifdef MAXIMALPATTERN
			}
#endif
			//if( pi->size == 2) gb->removeEdge(pi->mycost.intValue());
			//for filling edges
#ifdef TREEBASEDSCAN
#ifdef TIMERFLAG 
			Timers->start_timer(TIMER9);
#endif
#if    (defined MAXIMALPATTERN  && (MAXIMALPATTERN & 1)  )
			if(  ! GETMAXIMALBIT(pi->optimal) ) 
#endif
				pi->frequentElement( results, freq, totalInside);
#ifdef TIMERFLAG 
			Timers->stop_timer(TIMER9);
#endif
#endif
		}

		delete pi; /*definitely need to be taken care of*/
	}
}

	
void pattern:: constructM (void){
	char node1 = (char) mycost.index();
	char node2 = mycost.nlabel();
	char edge  = mycost.elabel();
	M = new AdjMatrix();
	M->addNode(node1);
	M->addNode(node2);
	M->addEdge(0, 1, edge);
	//M->buildAdjList();
}

void pattern:: fixM(void){

	int index = mycost.index();
	GLTYPE nlabel;

	int s1 = par->getSize();
	if( s1 == size){
		index -= s1;
		nlabel = EMPTY_EDGE;
	}
	else
		nlabel = mycost.nlabel();

	index = size-2-index;
	//M = new AdjMatrix(M, index, mycost.elabel(), nlabel );
	M = new AdjMatrix(M, size-1, index, mycost.elabel(), nlabel );
	stage &= 1;
}	

void pattern::setChild(EDGES & cm, EDGES & nm)
{
	EDGES::iterator ip;
	
	vector<int> u;

	ip = cm.begin();
	int v = COSTINDEX(ip->first), v1, k;
	u.push_back(ip->first);
	ip++;

	//search for those edges sharing the start node
	for( ; ip!= cm.end() ; ip++){
		k = ip->first;
		v1 = COSTINDEX(k);	
		if( v1 == v)
			u.push_back(k);
		else{
			child.push_back(setUpChild(u, cm, nm, v));
			u.clear();
			u.push_back(k);
			v = v1;
		}
	}
	child.push_back(setUpChild(u, cm, nm, v));
}

pattern * pattern :: setUpChild(vector<int> u, EDGES & edges, EDGES & gnodes, int nlabel)
{
	pattern * p1 = new pattern(this, 0, 1), * p2;
	CHLTYPE & c1 = p1->child;

	TRA lmapper;
	//populate the lmapper
	OCCS * es = gnodes[nlabel];
	OCCS::iterator oip = es->begin();
	int index =0;
	for(; oip != es->end(); oip++){
		occur * oc = *oip;
		DLONG key = MAKEKEY(INULLINDEX, oc->getGraph(), oc->getNodes(0));
		if( lmapper.count(key) ) error("duplicated key");
		lmapper[key] = index++;
		p1->coccs->push_back(key);
	}

	int i;
	for(i=0; i< u.size(); i++){
		es = edges[u[i]];
		p2 = new pattern(p1, u[i], 2);
		p2->constructM();
		setUpEdges(p2, es, lmapper);

#ifdef DEBUG5
		cout << "p2 " << endl;
		p2->print();
#endif
		c1.push_back(p2);
	}


#ifdef DEBUG5
	cout << "p1 " << endl;
	p1->print();
#endif

	return p1;
}

void pattern :: setUpEdges(pattern * p2, OCCS * es, TRA & tra1)
{
	//set up for the p2(edge)
	COCCS * cos2 = p2->coccs;	
	GRAPHS1 & gid2 = p2->gids;

	int gi, d1, d2, t1, j;
	DLONG key;

	//every edge is frequent and in carnonical form
	p2->optimal = 1;

	for(j=0; j< es->size(); j++){
		occur * noc = (*es)[j];
		gi = noc->getGraph();
		d1 = noc->getNodes(0), d2 = noc->getNodes(1);

		key = MAKEKEY(INULLINDEX, gi, d1);
		
		if( !tra1.count(key) ) error("the hash at p1 is not setting up");
		t1 = tra1[key];
		
		cos2->push_back(MAKEKEY(t1, gi, d2));

		//for(int m=0; m< gid2.size(); m++){
		//	if( gid2[m] > gi) error("why redundant graphs");
		if( !gid2.size() || gi != gid2[gid2.size()-1] ) gid2.push_back(gi);
		//}
		//if( !gid2.size() ) gid2.push_back(gi);
	}
}

void pattern :: join(pattern * p2, int freq){
	cost1 c2 = p2->mycost;
	int index1 = mycost.index(),node1 = mycost.nlabel();
	int index2 = c2.index(),	node2 = c2.nlabel();
	
	//check the intersection
	int sum=0;
	GRAPHS1 ::iterator iip1 = gids.begin(), iip2 = p2->gids.begin();
    while( iip1 != gids.end() && iip2 != p2->gids.end() ){
		if( *iip1 == * iip2){ iip1++; iip2++; sum++;}
		else if( * iip1 > *iip2) *iip2++;
		else iip1++;
	}

	//no way to be frequent
	if( sum < freq ) return;

	//test carnonical form first or occurrence first
	bool mf = true;
	if( size > 5) mf = false;

	//get the type of the join
	pattern * p3 = NULL;;
	int t = par->getSize(), type;

	//special treatment only for the first edge
	if( size == 2) index1 = index2 = 0;

	if( index2 >= t) {  //inner + inner  //notice it is okay for look back
		if( index1 != index2  && node1  == node2 ){
			p3 = new pattern(this, c2.intValue(), size);
			type = 1;		
		}
	}
	else if( index1 >= t){ //inner + outer
		p3 = new pattern(this, c2.intValue(), size+1);
		type = 2;
	}
	else{
		if( index1 != index2 ){
#ifndef TREEBASEDSCAN
			if( node1 == node2 ){
				int c = ((size+index2) << 16 ) | (c2.elabel() << 8 ) | node2;
				p3 = new pattern(this, c, size);
				type = 3;
				if ( join1(p2, p3, type, freq, mf) ) child.push_back(p3);
				else {
					delete p3 ; /*definitely need to be taken care of*/
				}
			}
#endif
		}
		else{
			if( mycost.intValue()> c2.intValue() && p2->optimal){
				//the only case we might switch the order
				p2->join(this, freq);
			}
		}
		
		
		int c = ((1+index2) << 16 ) | (c2.elabel() << 8 ) | node2;
		p3 = new pattern(this, c, size+1);
		type = 4;
	}

	//test for inner join of occurrences
	if ( p3 )
		if(join1(p2, p3, type, freq, mf) ){
			if( type == 4)
				newchild.push_back(p3);
			else
				child.push_back(p3);
		}
		else{
			delete p3; /*definitely need to be taken care of*/
		}
	}

bool pattern :: join1(pattern * p2, pattern * p3, int type, int freq, bool mf)
{


	//test carnonical form is mf is marked
	int t;
	bool carno = false;
	bool support = false;

#ifdef REGISTERFLAG
	if( !support){
		Registers->peg(REGISTER1);
	}
#endif

	if( mf) {
		t = p3->isCarnonicalForm();
		if( t< 0) {
			carno = false;
		}
		else{
			if( t> 0 ) p3->optimal = 1;
			carno = true;   //for optimal and suboptimal
		}
	}

	//get the inner join of occurrences
#ifdef TIMERFLAG
	Timers->start_timer(TIMER3);
#endif

	if( !mf || carno ){
		if( (type & 1)  ){
			support = propose_inner(p2, p3, freq);
		}
		else if( type  == 2){
			support = propose_inner_outer(p2, p3, freq);
		}
		else{
			support = propose_outer2(p2, p3, freq);
		}
	}
	
#ifdef REGISTERFLAG
	if( !support){
		Registers->peg(REGISTER2);
	}
#endif

#ifdef TIMERFLAG
	Timers->stop_timer(TIMER3);
#endif

	//if frequent, to see whether in carnonical form or not
	if( !mf && support) {
		t = p3->isCarnonicalForm();

		if( t< 0){ carno = false;  SETCLOSEDBIT(optimal); } //added by Luke for Maximal
		else{
			if( t> 0 ) p3->optimal = 1;
			carno = true;
		}
	}

#ifdef DEBUG2
		cout << "type : carnonical form and support test: " << type << "  " << t << " " << support << endl;
#endif

	//passed everything
	if( !carno || !support ) return false;
	else{
		return true;
	}
}

//for inner + inner or outer + outer `case 1
bool pattern :: propose_inner(pattern * p2, pattern * p3, int freq)
{
	//set up
	COCCS :: iterator ip1, ip2;
	COCCS * cos2 = p2->coccs;
	ip1 = coccs->begin(); ip2 = cos2->begin();

	COCCS * cos3 = p3->coccs;		
	GRAPHS1 & gid3 = p3->gids;

	int index =0; 
	bool flag = true;

	while(  ip1 != coccs->end()  && ip2 != cos2->end() ){
	
		if( *ip1 == *ip2 ){
		
			int gi = SECONDKEY(*ip1);
			cos3->push_back(MAKEKEY(index, gi, GNULLINDEX));
			//graph3->insert(gi);
			
			//update
			if( !gid3.size()  || gi != gid3[gid3.size()-1]) gid3.push_back(gi);
			//end

			ip1++; index++;	ip2++;
		}
		else if( *ip1 > *ip2 ){
			ip2++; flag = false;
		}
		else{
			ip1++; index++;
		}
	
	}

#if (defined MAXIMALPATTERN && (MAXIMALPATTERN & 1) )
	if( flag ){
		p2->optimal=0; //added by Luke for Maximal
#ifdef DEBUG11
		cout << "this is one absorbing "<< endl;
#endif
	}
#endif

	if( p3->sup() >= freq) {
		p3->stage = p3->stage | 2;
		return true;
	}
	else return false;
}

//for inner + outer 
bool pattern :: propose_inner_outer(pattern * p2, pattern * p3, int freq)
{
	//set up
	COCCS :: iterator ip1, ip2, ips, ipe;
	COCCS * cos2 = p2->coccs;
	ip1 = coccs->begin(); ip2 = cos2->begin();

	COCCS * cos3 = p3->coccs;		
	GRAPHS1 & gid3 = p3->gids;

	int index = 0, t, k;
	bool flag = true;

	while( ip1 != coccs->end()  && ip2 != cos2->end() ){

		t = FIRSTKEY(*ip1);
		k = FIRSTKEY(*ip2);

		if( t == k ){

			//find the ipe
			ips = ipe = ip2;
			while( ipe != cos2->end() &&  ( FIRSTKEY(*ipe) == t ) ) ipe++;

			int gi = SECONDKEY(*ip1);
			//graph3->insert(gi);
			
			//update
			if( !gid3.size() || gi != gid3[gid3.size()-1]) gid3.push_back(gi);
			//end

			for( ; ip2 != ipe; ip2++){
				cos3->push_back(MAKEKEY(index, gi, THIRDKEY(*ip2)));
			}

			ip1++;	index++; 
			
		}
		else if( t > k ){
			ip2++;  flag = false; 
		}
		else{
			ip1++; index++;
		}
	
	}

#if ( defined MAXIMALPATTERN && (MAXIMALPATTERN  & 1) )
	if( flag ){
		p2->optimal=0; //added by Luke for Maximal
#ifdef DEBUG11
		cout << "this is one absorbing in inner_outer "<< endl;
#endif
	}
#endif

	if( p3->sup() >= freq) {
		p3->stage = p3->stage | 2;
		return true;
	}
	else return false;
}

//for outer + outer case 2
bool pattern :: propose_outer2(pattern * p2, pattern * p3, int freq)
{
	COCCS :: iterator ip1, ip2, ips, ipe;
	COCCS * cos2 = p2->coccs;
	ip1 = coccs->begin(); ip2 = cos2->begin();

	COCCS * cos3 = p3->coccs;
	GRAPHS1 & gid3 = p3->gids;

	int index = 0, t, k;

	while( ip1 != coccs->end()  && ip2 != cos2->end() ){

		t = FIRSTKEY(*ip1);
		k = FIRSTKEY(*ip2);

		if( t == k ){

			//find the ipe
			ips = ipe = ip2;
			while( ipe != cos2->end() &&  ( FIRSTKEY(*ipe) == t ) ) ipe++;

			int gi = SECONDKEY(*ip1);

			bool flag = false;
			for( ; ip2 != ipe; ip2++){
				if( *ip1 !=  *ip2){
					cos3->push_back(MAKEKEY(index, gi, THIRDKEY(*ip2)));
					flag = true;
				}
			}

			//if( flag ) graph3->insert(gi);
			if( flag ) {	
				//update
				if( !gid3.size() || gi != gid3[gid3.size()-1]) gid3.push_back(gi);
				//end
#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
				//TURNON(p3->myTids, ip1- coccs->begin());  //added by Luke for TreeBased Scanning
#endif		
			}
	
			ip1++;	index++; ip2 = ips;
			
		}
		else if( t > k ){
			ip2++; 
		}
		else{
			ip1++; index++;
		}
	
	}

	if( p3->sup() >= freq) {
		p3->stage = p3->stage | 2;
		return true;
	}
	else return false;
}

bool pattern:: proposeOuterChild(int freq, vector<pattern*> & outChild){
	 
#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
	ELEFRENQC  counter;
	vector<int> ref;
	M->getBoundary(ref);
#endif

	//M->print();

	int x, key;
	map<int, pattern * , greater<int> > children;
	int elabel, nlabel;
	AdjMatrix * gbi;
	pattern * p3;
	vector<GNSIZE> * nei;
	vector<GNSIZE> :: iterator nip ;

	SCANOCCURRENCES   //macro for scanning occurrences

#ifdef DEBUG10
		vector<GNSIZE> :: iterator  fip = filter.begin();
		cout << "the nodes in the filter " << i << endl;
		for( fip = filter.begin(); fip != filter.end(); fip++){
			cout << (int) *fip << "\t" ;
		}
		cout << endl;
#endif

		//get the neighbors
		gbi = gb->graph(gi);

		//for debugging only
		 nei = gbi->getNeighbors(skey);
		
		//propose child
		for( j=0; j< nei->size(); j++){
			x = (*nei)[j];

			elabel = gbi->getLabel(skey, x);  nlabel = gbi->getLabel(x, x); 
			key = elabel << 8 | nlabel;
			if( (nip = find(filter.begin(), filter.end(), x)) == filter.end()  /*&& 
					gbi->getLabel(skey, x) != EMPTY_EDGE*/ ){
#ifdef REGISTERFLAG 
				Registers->peg(REGISTER3);
#endif
				if( !children.count(key) ){
					children[key] = new pattern(this, key, size+1);
				}	
				p3 = children[key];
				if( !p3->gids.size() || gi != p3->gids[p3->gids.size()-1]) p3->gids.push_back(gi);
				p3->coccs->push_back(MAKEKEY(i, gi, x));
			}
#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
			else if( size > 2 && 
				M->getLabel( (size-1), (int) (nip - filter.begin()) ) == EMPTY_EDGE){
				key = (size -1) << 24 | ((int) (nip - filter.begin()) ) << 16 | key;
				if( ! counter.count(key) )  counter[key] = new IIDTYPE((coccs->size()+31)/32);
				TURNON( *(counter[key]), i);
			}
#endif
		}
	 }

#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
#ifdef TIMERFLAG
	Timers->start_timer(TIMER8);
#endif
	int treeClosed = scanElements(counter, freq, ref);
#ifdef TIMERFLAG
	Timers->stop_timer(TIMER8);
#endif
#endif

#if (defined MAXIMALPATTERN && (MAXIMALPATTERN & 1) )
	
	if( treeClosed  < 0 ){  //there is one strongly associated with the current pattern and the canonical form is larger
		map<int, pattern * , greater<int> > ::iterator cit = children.begin();
		for( ; cit != children.end(); cit++)  delete cit->second;
		return false;
	}
	//else if ( !treeClosed ) optimal |= 4; //set the 3nd bit on, which suggest that there is 
#endif

	//scan children to see whether it is frequent and optimal 
	scan(children,freq, outChild);
	return true;
}

void pattern:: scan(map<int, pattern*, greater<int> > & cand, int f, vector<pattern*> & result)
{

	map<int, pattern*, greater<int> > :: iterator ip;
	
	for(ip = cand.begin(); ip!= cand.end(); ip++){
		pattern *  p = ip->second;

#ifdef REGISTERFLAG 
		Registers->peg(REGISTER8);
#endif
		if( p->sup() < f ) { 

#ifdef REGISTERFLAG 
			Registers->peg(REGISTER7);
#endif

#ifdef DEBUG2
			cout << "failed support test " << endl;
#endif
			delete p;  /*definitely need to be taken care of*/
		}
		else {
			int t = p->isCarnonicalForm();

#ifdef DEBUG2
			cout << "the scan test and t value: " << t << endl;
#endif
			if( t < 0 ) {

#ifdef REGISTERFLAG 
				Registers->peg(REGISTER6);
#endif
				delete p;	 /*definitely need to be taken care of*/
				SETCLOSEDBIT(optimal); /*added by Luke for maixmal*/
			}
			else {
				p->optimal = t;
				//if( t == 1)   p->fixOccs();
				result.push_back(p);

#ifdef REGISTERFLAG 
				if( !t )
					Registers->peg(REGISTER5);
				else			
					Registers->peg(REGISTER4);
#endif
			}
		}

	}
}

//save results
#ifdef SAVERESULTS
void pattern:: recordOccs(vector<occur*> & loccs)
{

	SCANOCCURRENCES
		loccs.push_back(new occur(filter, gi));
	}
}
#endif


#ifdef GSPANFLAG
void pattern :: findAllIsomorphisms(){
	GRAPHS ::iterator ip = graphs->begin();
	
	int sum=0;

	for(; ip!= graphs->end(); ip++){
		//cout << *ip << endl;
		AdjMatrix * tg = gb->graph(*ip);
		sum += tg->findAllIsomophisms(M);
	}

	if( sum != occs->size()){
		cout << "sum:" << sum << "\t" << occs->size();
		M->print();
		error("couting error");
	}
}
#endif

#ifdef MAXIMALPATTERN
bool pattern :: isMaximal(){

	if( innerCandidates.size() || innerOuterCandidates.size() ) return false;
	else return true; 
}

bool pattern :: isMaximal(AdjMatrix * CM1, IIDTYPE * local, int freq )
{
#ifdef DEBUG17
	//scanning cross internal and external 
	cout << "checking maximallity" << endl;
	OUTPUTIID(*local);
	CM1->print();
#endif

	bool flag = true;
	FREELE ::iterator ip;

	for( ip= innerOuterCandidates.begin(); ip != innerOuterCandidates.end(); ip++){
		//OUTPUTIID( *(*ip));
		if( getSupport(local, *ip) >= freq ){
			flag = false; break;
		}
	}
#ifdef DEBUG17
		cout << "inner out" << flag << endl;
#endif

	//check inner candidates only
	if( flag){
		//cout << "inner size " << innerCandidates.size() << endl;
		int k1, k2;
		IIDTYPE * ele;
		for( ip = innerCandidates.begin(); ip != innerCandidates.end(); ip++){
			ele = *ip;
			//OUTPUTIID(*ele);
			//cout << "key " << hex << ((*ele)[ele->size()-2]) << dec << endl;
			k1 = ((*ele)[ele->size()-2]) >> 24; 
			k2 = ( ( ((*ele)[ele->size()-2]) ) >> 16) & 0x000000ff;
			//cout << k1 << " " << k2 << " " << CM1->getLabel(k1, k2) << endl;
			if( CM1->getLabel(k1, k2) == EMPTY_EDGE && ( getSupport(local, ele) >= freq ) ) {
				flag = false; break;
			}
		}
	}

	//check external edges only
	if( flag ){
		flag = isMaximal(freq, false, local);  
	}
#ifdef DEBUG17
	cout << "result " << flag << endl;
#endif
	return flag;
}

bool pattern :: isMaximal(int threshold, bool all, IIDTYPE * Tid){

	CGOCC localCounter;

	SCANOCCURRENCES
		if( (all || GETTID(*Tid, i) ) && 
			countSuperGraph(filter, gi, localCounter, threshold) )	return false;
	}

	return true;
}

bool pattern :: countSuperGraph(vector<GNSIZE> & nodes, int gi, CGOCC & counter, int threshold){

	AdjMatrix * gbi = gb->graph(gi);


	//inner checking
	int n = nodes.size()-1;
#ifdef SAFEMODE
	if( n >= 127 ) error("pattern size is too large (> byte)");
#endif

	int key, value, i, j;
	vector<GNSIZE> * nei;
	//vector<GNSIZE> :: iterator nip;

	/*for( i=1; i<= n; i++){
		nei = gbi->getNeighbors(nodes[i]);
		for( k=0; k< nei->size(); k++){
			if( (j = (int) (find(nodes.begin(), nodes.end(), (*nei)[k]) - nodes.begin() ) ) < i &&
				M->getLabel(i,j) == EMPTY_EDGE){
				c = gbi->getLabel(nodes[i], nodes[j]);
				key = (i << 24) | (j << 16) | 0 | c;
				if( !counter.count(key) ){
					counter[key] = ( (gi<<16) | 1);
					//if( 1 >= threshold) return true;
				}
				value = counter[key];
				if( (value >> 16) != gi) {
					counter[key] =  (gi<<16) | ( (++value) & 0x0000ffff ); 
					if( (value & 0x0000ffff)  >= threshold ) return true;
				}
			}
		}
	}
	*/

	//outer checking
	int x;
	for( i=0; i<= n; i++){
		 nei = gbi->getNeighbors(nodes[i]);
		
		//propose child
		for( j=0; j< nei->size(); j++){
			x = (*nei)[j];
			if( (find(nodes.begin(), nodes.end(), x) == nodes.end() ) ){
				key = 	((n+1) << 24) | (i<<16) | (gbi->getLabel(x, nodes[i]) << 8 )
					| gbi->getLabel(x,x)  ;
				//cout << " key " << hex << key << dec << endl;
				if( !counter.count(key) ){
					counter[key] = ( (gi<<16) | 1);
					if( 1 >= threshold) return true;
				}
				value = counter[key];
				if( value >> 16 != gi){
					counter[key] =  (gi<<16) | (( ++value ) & 0x0000ffff );
					if( (value & 0x0000ffff) >= threshold ) return true;
				}
			}
		}
	}

	return false;
}
#endif

void pattern:: frequentElement(vector<AdjMatrix*> & results, int freq, int & totalnum){
	
#ifdef DEBUG14
	M->print();
	int i;
	cout <<"inner candidates size " << innerCandidates.size() << endl;
	for( i =0; i< innerCandidates.size(); i++){
		IIDTYPE * tid = innerCandidates[i];
		for( int j=0; j< tid->size(); j++)
			cout << hex << (*tid)[j] << dec << " " ;
		cout << endl;
	}
	cout <<"outer candidates size " << outerCandidates.size() << endl;
	for( i =0; i< outerCandidates.size(); i++){
		IIDTYPE * tid = outerCandidates[i];
		for( int j=0; j< tid->size(); j++)
			cout << hex << (*tid)[j] << dec << " " ;
		cout << endl;
	}
#endif
	//add to true patterns
	vector<AdjMatrix*> localResults;
	FREELE local(innerCandidates);  //make a copy of it
	enuFreqEle(local, localResults,  freq, totalnum);
	results.insert(results.end(), localResults.begin(), localResults.end());

}

void pattern:: enuFreqEle(FREELE & candidates, vector<AdjMatrix*> & results , int freq, int & totalnum)
{
	//vector<pattern*> & results;
	//remove abosoluted associated elements from candidates
	IIDTYPE Tids((coccs->size()+31)/32+2, -1) ;
	depthSearch(M, &Tids, candidates, results, freq, totalnum);
}
	
bool pattern:: lookAhead( AdjMatrix * CM, const IIDTYPE * Tids, const FREELE & candidates, 
						   vector<AdjMatrix*> & results , int freq, int & totalnum)
{
	
	IIDTYPE local(*Tids);  
	
	int i, j, key;
	GLTYPE el;
	bool flag = true;

	AdjMatrix  *CM1 = new AdjMatrix(CM);
	FREELE ::const_iterator ip = candidates.begin();

#ifdef DEBUG18
	cout << "entering look ahead" << endl;
	CM->print();
#endif

	for( ; ip!= candidates.end() && flag ; ip++){
		key = (*(*ip))[(*ip)->size()-2];

		//OUTPUTIID(*(*ip));
		i = FREFIRSTKEY(key); j = FRESECONDKEY(key); el = FRETHIRDKEY(key);

		if( CM->getLabel(i, j) == EMPTY_EDGE) { //no collisiion
			intersectInstances(&local, Tids);
			if( getSupport(&local) < freq ) flag = false; 
			else{ 
				if( CM1->getLabel(i, j) != EMPTY_EDGE) error("no way to happend for edge colission");
				CM1->addEdge(i, j, el);
			}
		}
	}

	if( flag ){ //all are frequent 
#ifdef DEBUG18
		CM1->print();
#endif
		if( (CM1->isCanonicalForm() ==1 ) /* && isMaximal(CM1, &local, freq)*/ ){
#ifdef SAVERESULTS
			if( SAVERESULTS == 2){
				GRAPHS1 localgids;	obtainSupportVector(&local, localgids); 
				CM1->setGraphIDs(localgids);
			}
			//cout << "saving result" << endl;
			results.push_back(CM1); 
			totalnum++;
			CM1->print();
#else
			delete CM1;
#endif
		}
	}
	else delete CM1;
#ifdef DEBUG18
	cout << "look ahead result" << flag << endl;
#endif
	return flag;
}

/*
	true: for no new result false: new result added
*/
bool pattern:: depthSearch(AdjMatrix * CM, IIDTYPE * Tids, FREELE & candidates, 
						   vector<AdjMatrix*> & results , int freq, int & totalnum)
{
	if( !candidates.size() ) return true;

	//try to look ahead
#if (defined MAXIMALPATTERN && (MAXIMALPATTERN & 2) )
	if( lookAhead(CM, Tids, candidates, results, freq, totalnum) ) return false;
#endif

	//construct CM'
	IIDTYPE * local = candidates.back();  candidates.pop_back();
	IIDTYPE * local1 = new IIDTYPE(*local);
	intersectInstances(local1, Tids);
	bool flag = true;

	//remove abosoluted associated elements
	//FREELE candidates1(candidates.being(), candidates.end());

	if( getSupport(local1) >= freq ){
		flag = false;
		int key = (*local)[local1->size()-2];
		int i = FREFIRSTKEY(key), j = FRESECONDKEY(key),  lr;
		GLTYPE c =  FRETHIRDKEY(key);
		AdjMatrix * CM1 = new AdjMatrix(CM, i, j, c, EMPTY_EDGE);

#ifdef DEBUG14
		cout << "checking candidates" << hex << key << dec << endl;
#endif

		bool lm = true;
		//if( (lr = CM1->isTreeBasedCanonicalForm()) > -2 )  //if is -2 then no need to search anymore
#ifdef MAXIMALPATTERN
		if( (lr = CM1->isCanonicalForm()) > -1 )   //candidates sort by indices, assumed 
#else
		if( (lr = CM1->isCanonicalForm()) > -2 )	//only -2, -1, 1
#endif
			lm = depthSearch(CM1, local1, candidates, results, freq, totalnum);  

		if( lr == 1 ){ 
			
			//if we asking for maximal, we need to set additional check here
			//totalnum++;

#ifdef MAXIMALPATTERN 
			if( lm && isMaximal(CM1, local1, freq) ) {
#endif
				//cout <<"total num " << totalnum;
				totalnum++;

#ifdef SAVERESULTS
				if( SAVERESULTS == 2){
					GRAPHS1 localgids;	obtainSupportVector(local1, localgids); 
					CM1->setGraphIDs(localgids);
				}
				results.push_back(CM1); 
#else
				delete CM1;
#endif

#ifdef MAXIMALPATTERN
			}
			else delete CM1;
#endif
		}
		else delete CM1;  //not in canonical form
#ifdef DEBUG14
		cout << "successing " << lr << endl;
#endif
	}
	flag &= depthSearch(CM, Tids, candidates, results, freq, totalnum);
	candidates.push_back(local);  
	delete local1;
	
	return flag;
}

/*
return -1: for external strongly associated edges, which breaks the canonical form, not fully implemented yet
       0 : external strongly associated edge, which don't break the cannonical form, not implemented yet
	   1 : normal 
*/
int pattern:: scanElements(ELEFRENQC  & counter , int freq, vector<int> & ref)
{	
    //post processing
    ELEFRENQC ::iterator eip = counter.begin();
	IIDTYPE * tids;
	int sup, i, j, key;

#ifdef DEBUG16
	M->print();
#endif

	for( ; eip != counter.end(); eip++){
		tids = eip->second;
#ifdef DEBUG16
		cout << "key " << hex << eip->first << dec;
#endif
		if( (sup = getSupport(tids) ) >= freq )
		{
			key = eip->first;
			tids->push_back(key);
			tids->push_back(sup);
			i = FREFIRSTKEY(key); j = FRESECONDKEY(key);
			if( i < size ){
				if( j > ref[i] )
					innerCandidates.push_back(tids);
				else if( j < ref[i])
					innerOuterCandidates.push_back(tids);
			}
			/*   //should not happen at all
			else if( i >= size)
				outerCandidates.push_back(tids);
			*/
		}
		else
			delete tids;
#ifdef DEBUG16
		cout << "sup " << sup << endl;
#endif
	}

	//download the frequent elements from parant
	//cout << "size " << size << endl;
	FREELE :: iterator pip;
	bool flag= false; int value =1;

	if( size  > 2){
		for( pip = par->innerOuterCandidates.begin(); 
		pip != par->innerOuterCandidates.end(); pip++){
			flag = makeCompack(*pip, innerOuterCandidates, freq);  //true for strongly associated 
#ifdef MAXIMALPATTERN
			if( flag ) { 
				value = -1;  break; 
			} 
#endif
		}

		if(  value != -1 ){ //not strongly associated yet
			for( pip = par->innerCandidates.begin(); 
			pip != par->innerCandidates.end(); pip++){
				flag = makeCompack(*pip, innerCandidates, freq);
			}
		}
	}

	//sorting the element
#ifdef MAXIMALPATTERN
	if( size > 2 && (value != -1) ) sortCandidates(innerCandidates, innerOuterCandidates);
#endif

#ifdef DEBUG16
	cout << "candidates size " << innerCandidates.size() << endl;
	for( pip = innerCandidates.begin();pip != innerCandidates.end(); pip++){
		OUTPUTIID(*(*pip));
	}
	cout << "monitor candidates size " << innerOuterCandidates.size() << endl;
	for( pip = innerOuterCandidates.begin();pip != innerOuterCandidates.end(); pip++){
		OUTPUTIID(*(*pip));
	}
#endif

	//sorting the candidates by appearans in search
	return value;
}

void pattern :: addInstances(ELEFRENQC & counter, vector<GNSIZE> & nodes, int gi, int instanceID, vector<int> & ref)
{
	AdjMatrix * gbi = gb->graph(gi);
	int i, k,j, key=0;
	GLTYPE c;
	vector<GNSIZE> * nei;
	int n = nodes.size()-1;
	IIDTYPE *  value;
	bool flag;
	//vector<GNSIZE> :: iterator lip;

	//cout << "in" << endl;
	for( j=0; j<= n; j++){
		nei = gbi->getNeighbors(nodes[j]);
		for( k=0; k< nei->size(); k++){
			flag = false;
			i = (int) ((find(nodes.begin(), nodes.end(), (*nei)[k])) - nodes.begin() );
			c = gbi->getLabel( (*nei)[k], nodes[j]);
			/*if( i > n ){  //outside the matrix
				key = (i << 24) | (j << 16) | (c << 8) | (gbi->getLabel((*nei)[k], (*nei)[k]));
				flag = true;
			}
			else */ if ( i<=n && i > j && j != ref[i] ){  //inside the matrix
				key = (i << 24) | (j << 16) | (c << 8) | (gbi->getLabel((*nei)[k], (*nei)[k]));
				flag = true;
			}
#ifdef DEBUG16
			if( flag ) cout << "i in out " << i << " j is " << j << " key  " << hex << key << dec << endl;
#endif
			if( flag ){
				if( !counter.count(key) )  //initialization
					counter[key] = new IIDTYPE( (coccs->size()+31)/32 );  //initialize with zero
				value = counter[key];
				TURNON(*value, instanceID);
			}
		}
	}
}

/*
	true: strongly associated
	false: not strongly associated
*/
bool pattern :: makeCompack(IIDTYPE * ele, FREELE & dest, int freq)
{
	IIDTYPE local((coccs->size()+31)/32);

#ifdef DEBUG16
	OUTPUTIID(*ele);
#endif

	int gi =-1, gii, sup = 0;
	bool flag = true; 

	for(int i=0; i< coccs->size(); i++){
		if( GETTID(*ele, FIRSTKEY((*coccs)[i]) ) ){
			TURNON(local, i);
			if( (gii= SECONDKEY((*coccs)[i])) != gi ){
				sup++;
				gi = gii;
			}
		}
		else flag = false;
	}
	if( sup >= freq ){
		local.push_back( (*ele)[ele->size()-2] );
		local.push_back(sup);
		dest.push_back(new IIDTYPE(local));
	}
#ifdef DEBUG16
	cout << "strongly associationg results" << flag << endl;
#endif

	return flag;
}

void pattern :: intersectInstances(IIDTYPE * ele1, const IIDTYPE * ele2){
	
	for(int i=0; i< ele1->size()-2; i++)		(*ele1)[i] &= (*ele2)[i];
}

int pattern :: getSupport(IIDTYPE * ele1, IIDTYPE * ele2){

	IIDTYPE local(*ele1);
	intersectInstances(&local, ele2);
	return getSupport(&local);
}

int  pattern:: getSupport( IIDTYPE * iid)
{
	int sup = 0;
	int gi=-1;
	for(int i=0; i< coccs->size(); i++){
		if( GETTID(*iid, i) ){
			if( gi != SECONDKEY((*coccs)[i]) ){
				gi = SECONDKEY((*coccs)[i]);
				sup++;
			}
		}
	}
#ifdef DEBUG14
	cout << "sup " << sup << endl;
#endif
	return sup;
}

void  pattern:: obtainSupportVector( IIDTYPE * iid, GRAPHS1 & gids)
{
	int gi=-1;
	for(int i=0; i< coccs->size(); i++){
		if( GETTID(*iid, i) ){
			if( gi != SECONDKEY((*coccs)[i]) ){
				gi = SECONDKEY((*coccs)[i]);
				gids.push_back(gi);
			}
		}
	}
}

bool CompPos :: operator()( IIDTYPE * const & oc1,  IIDTYPE * const & oc2)
{
	//bool flag = false;
	
	//IIDTYPE * ele = *oc1, ele2 = *oc2;
	if( oc1 == NULL || oc1->size() < 2 || oc2->size() < 2) error("ruppted tidste");

	int k1 = (*oc1)[oc1->size()-2] >> 16, k2 = (*oc2)[oc2->size()-2] >> 16;
	//if( k1 <= k2 ) flag = true;
	return !( k1 <= k2);

	//return flag;
}

bool CompSup :: operator()( IIDTYPE * const & oc1,  IIDTYPE * const & oc2)
{
	//bool flag = false;
	//IIDTYPE * ele = *co1, ele2 = *oc2;
	if( oc1 == NULL || oc1->size() < 1 || oc2->size() < 1) error("ruppted tidste");
	return ( oc1->back() >= oc2->back() );
	//return flag;
}

void pattern :: sortCandidates(FREELE & candidates1, FREELE & candidates2)
{
	//cout << ".";
	sort(candidates1.begin(), candidates1.end(), CompPos());
	//sort(candidates2.begin(), candidates2.end(), CompSup());
	//cout << "|";
}

