/********************************************************************
 * adj_matrix.cpp
 * Implementation of the class AdjMatrix
 *
 ********************************************************************/
#pragma warning (disable:4786 4018 4267)
#include <iostream>
#include <assert.h>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
using namespace std;

#include "adj_matrix.h"
#include "common.h"
#include "Ullman.h"
#if defined (VFLIB)
	#include "argraph.h"
	#include "argedit.h"
	#include "attribute.h"
	#include "match.h"
	#include "ull_sub_state.h"
	#include "vf2_sub_state.h"
	#include "vf2_state.h"
	#include "vf2_sub_state.h"
#endif

// fill in vector v with neighbors of node index
vector<GNSIZE> * AdjMatrix::getNeighbors(int index)
{
	return SAdjList[index];
}

void AdjMatrix::getNeighbors(int index, vector<GNSIZE> & v)
{
	copy(SAdjList[index]->begin(), SAdjList[index]->end(), v.begin());
}


bool AdjMatrix::isPath()
{
    int ends = 0;
	for (int i = 1; i < size(); i++) {
        int degree = 0;
        for (int j = 0; j < size(); j++) { 
            if ( getLabel(i,j) != EMPTY_EDGE) {
                    degree++;
                    if (degree > 3)
                        return false;
            }
        }
        if (degree == 2) ends++;
    }
    return ends == 2;
}

// fill in vector v with nodes with max label
void AdjMatrix::getMaxNode(vector<int> &v)
{
	GLTYPE max = getLabel(0,0);
	int i;
	for (i = 1; i < size(); i++) {
		if (max < getLabel(i,i))
			max = getLabel(i,i);
	}

	for (i = 0; i < size(); i++) {
		if (getLabel(i,i) == max)
			v.push_back(i);
	}
}

void AdjMatrix::getMaxNode2(vector<KEYTYPE> &v)
{
	GLTYPE max = getLabel(0,0);
	int i;
	for (i = 1; i < size(); i++) {
		if (max < getLabel(i,i))
			max = getLabel(i,i);
	}

	for (i = 0; i < size(); i++) {
		if (getLabel(i,i) == max)
			v.push_back(aMAKEKEY(aNULLKEY,i));
	}
}

//tree based canonical form
int AdjMatrix::isTreeBasedCanonicalForm(){
	
#ifdef DEBUG13
	print();
#endif

	vector<int> maxNodes;   getMaxNode(maxNodes);
	int i, value =1, temp;

	if( getLabel(maxNodes[0], maxNodes[0]) > getLabel(0,0)){
#ifdef DEBUG15
		cout << "first node not matching" << endl;
#endif
		value = -2;
	}

	if( value == 1 ){
		/*GLTYPE c; bool flag; 
		for(i=0; i< size(); i++){
			flag = true;
			for(int j=0; j<= i; j++){
				if( (c = getLabel(i, j)) != EMPTY_EDGE ){ 
					if( flag ) ref1.push_back(CANONICALELE(i,j, c, getLabel(i, i)));  //all edges
					ref2.push_back(CANONICALELE(i,j,0,c));
					flag = false;
				}
			}
		}*/
		vector<int> ref1, ref2;
		getReferrence(ref1);
		getCanonicalForm(ref2);
#ifdef SAFEMODE
		if( ref1.size() != size() ) error("ref size doesn't match");
#endif

		for(i=0; i< maxNodes.size(); i++){
			vector<GNSIZE> occ(1, maxNodes[i]);
			temp = isTBRecCanonicalForm(occ, ref1, ref2, 0, 1);
			value = value < temp? value : temp;
			if( value == -2 ) break;
		}
	}

#ifdef DEBUG13
	cout << "return value " << value << endl;
#endif
	return value;
}

void AdjMatrix :: getBoundary(vector<int> & ref){
	GLTYPE c;
	for(int i=0; i< size(); i++){
		for(int j=0; j<= i; j++){
			if( (c = getLabel(i, j)) != EMPTY_EDGE ){ 
				ref.push_back(j); break;
			}
		}
	}

}

void AdjMatrix :: getReferrence(vector<int> & ref){
	GLTYPE c;
	for(int i=0; i< size(); i++){
		for(int j=0; j<= i; j++){
			if( (c = getLabel(i, j)) != EMPTY_EDGE ){ 
				ref.push_back(CANONICALELE(i,j, c, getLabel(i, i))); break;
			}
		}
	}
}

void AdjMatrix :: getCanonicalForm(vector<int> & ref){
	GLTYPE c;
	for(int i=0; i< size(); i++){
		for(int j=0; j<= i; j++){
			if( (c = getLabel(i, j)) != EMPTY_EDGE ) 
				ref.push_back(CANONICALELE(i,j,0,c));
		}
	}
}

void AdjMatrix :: getCanonicalForm(vector<GNSIZE> & occ, vector<int> & ref){
	
	GLTYPE c;
	for(int i=0; i< occ.size(); i++){
		for(int j=0; j<= i; j++){
			if( (c = getLabel(occ[i], occ[j])) != EMPTY_EDGE ) 
				ref.push_back(CANONICALELE(i,j,0,c));
		}
	}
}

/*
   for tree: 1 canonical form
             0 suboptimal canonical form
   for graphs but not trees:  1  canonical form
                              -1 not canonical form, but maximal tree is canonical 
							  -2 maximal tree is not canonical
*/
int AdjMatrix::isTBRecCanonicalForm( vector<GNSIZE> & occ, vector<int> & ref , vector<int> & ref2, int pos1, int pos2){
	
	if( occ.size() == size() ){  //end of the search


		if(	esize() == (size()-1) ) return 1;     //for tree only 

		vector<int> ref3;
		getCanonicalForm(occ, ref3);

		//cout << "occ s are  " << endl;
		int i;
		//for(i=0; i< occ.size(); i++) cout << occ[i] << " " << endl;

#ifdef SAFEMODE
		if( ref2.size() != ref3.size() ) error("sizes of referrences are not matching");
#endif
		//cout << "size " << ref2.size() << endl;
		for( i=0; i< ref3.size(); i++) { 
			//cout << hex << ref3[i] << " ref 2 " << ref2[i] << dec << endl;
			if( ref3[i] > ref2[i]) return -1;
			else if( ref3[i] < ref2[i] ) break;
		}
		return 1;
	}

	int u, v, value = 1, temp;
	vector<GNSIZE> * nei;
	vector<GNSIZE> :: iterator ip;
	int key;
	bool flag = true;
	GLTYPE c1, c2;

	while( flag ){
		u = occ[pos1]; nei = getNeighbors(u);
		for( ip = nei->begin(); ip != nei->end(); ip++){
			if( find(occ.begin(), occ.end(), *ip) == occ.end() ){  //haven't been searched yet
				flag = false;  v= *ip;
				c1 = getLabel(u, v); c2 = getLabel(v, v);
				key = CANONICALELE(pos2, pos1, c1, c2);
				if( key > ref[pos2] ){
#ifdef DEBUG15
					cout << "key  " << hex << key << " ref value " << ref[pos2] << dec << endl;
#endif
					if( (size() == esize()+1)&& (occ.size() == (size()-1)) ) return 0;   //suboptimal and tree
					else return -2;
				}
				else if( key == ref[pos2]){
					occ.push_back(v);
					temp = isTBRecCanonicalForm(occ, ref, ref2, pos1, pos2+1);  //depth first search
					value = value < temp ? value : temp;
					occ.pop_back();
					if( value == -2){
#ifdef DEBUG15
						cout << "node list " << endl;
						for(int ii=0; ii< occ.size(); ii++) cout << occ[ii] << " ";
						cout << endl;
#endif
						return -2;
					}
				}
			}
		}
		pos1++;
	}
	return value;

}
int AdjMatrix::isTreeCanonicalForm(){
	return 0;
}

// return true if the matrix is canonical form

int AdjMatrix::isCanonicalForm(){
	
	//int t1 = isCanonicalForm1();
#ifdef TREEBASEDSCAN 
	//cout << "this" << endl;
	return isTreeBasedCanonicalForm();
	
#endif

	int t2 = isCanonicalForm2();
	return t2;
	/*if( t1 != t2 ){ 
		print();
		cout << " t1 t2 is " << t1 << "\t" << t2 << endl;
		error("mis matching in canonical form\n"); return 0;
	}
	else return t1;
	*/
	
} 

int AdjMatrix::isCanonicalForm2() 
{
	vector<KEYTYPE>   searchTree;
	vector<GNSIZE>    nodeTree;

	if( size() == 2) return 1;  // for symmetrix adj matrix
	vector<GNSIZE> ancesters(size(), 0);
	int i, j, k, m, n, u, v, w, uplimit1, uplimit2;

	// level 1(single node)
	getMaxNode2(searchTree);

	//cout << "search 2\n" << "search tree size" << searchTree.size() << endl;
	if (getLabel(0,0) != getLabel( aSECONDKEY(searchTree[0]), aSECONDKEY(searchTree[0])) )
		return -1;    //false -> change to -1

	//initialize the nodeTree
	for( m =0; m< searchTree.size(); m++){
		nodeTree.push_back(0);
	}

	//set up l
	int bi, bl;
	last2bitil(bi, bl);    //to set the second to last bit's index and level

	//level wise search
	uplimit1 = 0;
	int code = 1;
	for (i = 1; i < size(); i++) {
		
		// step 1. propse next level candidates to vector next
		uplimit2 = searchTree.size();
		vector<KEYTYPE> refValues;

		//populate the refValues;
		for(m=0; m<=i; m++){
			if( getLabel(i,m)!= EMPTY_EDGE )
				refValues.push_back(aMAKEKEY(i-m, getLabel(i,m)));
		}

		for (j = uplimit1; j < uplimit2; j++) {
			u = searchTree[j];
					
#ifdef DEBUG1
			cout << "level " << i << ", #" <<j<<": " << hex <<  u  << dec << endl;
#endif
			
			//get the ancesters of u
			ancesters[i-1] = aSECONDKEY(u);
			int u1 = u;
			for(m=1; m<i; m++){
				int fkey = aFIRSTKEY(u1);
				u1   = searchTree[fkey];
				ancesters[i-1-m] = aSECONDKEY(u1);
			}


//#ifdef TIMERFLAG
//			Timers.start_timer(TIMER2);
//#endif
			int outdegree = 0;
			for( n = nodeTree[j]; n < i && !outdegree; n++){
				
				//get neighbors
                vector<GNSIZE> * neighbor_vec = SAdjList[ancesters[n]];
	
				// step 2. scan for neighbors that havn't be searched before
				for( k=0; k< neighbor_vec->size(); k++){
					v = (*neighbor_vec)[k];
                    ancesters[i] = v;

					//string *buf = matrix[v] ;
					bool flag = true;

					//populate the cost
					int tcode = 1, mii =0;
					//cout << "code is " << code << "\n";
					if( find(ancesters.begin(), ancesters.begin()+i, v) 
                                               != (ancesters.begin()+i))
						flag = false;
					else{
					   for( m=0; m<=i && tcode==1 ; m++){
						  w = ancesters[m];
					      int refkey = getLabel(v, w);
						  if(  refkey != EMPTY_EDGE){
	
						     if( refValues[mii] < aMAKEKEY(i-m,  refkey )){
						        if( ((i-aFIRSTKEY(refValues[mii])) > bi && i == bl) || i > bl) tcode = 0;
						        else tcode = -1;  //no need to further search
									//cout << "value " << hex << refValues[mii] << " " << aMAKEKEY(i-m, (*buf)[w]) << dec << endl;
							 }
						     else if( refValues[mii] > aMAKEKEY(i-m, refkey) )tcode = 2;
						     mii++;
					       }
					   }
					}
					//cout << "code is " << code  << " " << tcode <<" \n";
					//cout << "flag is " << flag  << " " << v << "\n";

					if( flag ) {
						
						if( tcode < 0 ) return tcode;
						else if( tcode ==0 ) code =0;

						outdegree++;

						if( tcode == 1 ){
							searchTree.push_back(aMAKEKEY(j, v));
							nodeTree.push_back(n);
						}
					} //if( flag) know the result for sure
				}
			}
//#ifdef TIMERFLAG
//			Timers.stop_timer(TIMER2);
//#endif
		} //for (j = uplimit1; j < uplimit2; j++)  
		uplimit1 = uplimit2;
	}

	//assert(false); // never reach here
	return code;
}

#ifdef SAVERESULTS
void AdjMatrix::print(int pi, string f1, string f2, string f3, vector<int> & g_reindex)
{
    ofstream of3(f3.c_str(), ios::app);
    map<int, int> ::iterator ip;
    int j;
    for( ip = graphs.begin(); ip != graphs.end(); ip++){
         int gj = ip->first;
         int gi = -1;
         for( j=0; j<g_reindex.size(); j++)
            if ( gj == g_reindex[j] ){                                                                                   gi = j; break;                                                                                       }

         of3 << "f " << dec << pi << " " << gi << "  " << ip->second << endl;
     }
     of3.close();
     print(pi, f1, f2, g_reindex);
}

void AdjMatrix::print(int pi, string f1, string f2, vector<int> & g_reindex)
{
        //output the pattern
        ofstream of1(f1.c_str(), ios::app);
        int i, j;

        for ( i = 0; i < size(); i++){
          GLTYPE c = getLabel(i, i);
          of1 << "node " << dec << pi << " " << i << " " << (c-EMPTY_EDGE) << endl;
        }
        of1.close();

        //output the pattern
        ofstream of2(f2.c_str(), ios::app);
        for ( i = 1; i < size(); i++){
          for( j=0; j< i; j++){
             GLTYPE c = getLabel(i, j);
             if( c != EMPTY_EDGE)
                 of2 << "edge " << dec << pi << " " << i << " " << j << " " <<
				 (c-EMPTY_EDGE) << endl;
          }
        }
        of2.close();
}


void AdjMatrix::setGraphs(vector<occur*> & occs1)
{
	occs = occs1;
    for(int i=0; i< occs.size(); i++){
       int gi = occs[i]->getGraph();
       if( graphs.count(gi) )
         graphs[gi]++;
       else
         graphs[gi] = 1;

#ifdef SAVERESULTS
	   if( SAVERESULTS == 1 ) delete occs[i];
#endif
	}
	occs.clear();
}

void AdjMatrix :: setGraphIDs(vector<GNSIZE> & gids)
{
	graphIDs = gids;
}

void AdjMatrix :: setTIDs(GRAPHS1 & g,  int tsize)
{
	totalSup = g.size();
	int temp=0, j=0;
	for(int i=0; i< tsize; i++){
		if( j< totalSup && i == g[j] ){
			j++; temp++;
		}
		if( (i+1) % 32 == 0){ TIDs.push_back(temp); temp = 0; }
		else	temp = temp << 1;
	}
	TIDs.push_back(temp);
}
#endif

void AdjMatrix::print()
{
	//output the pattern
	int i, j;
	cout << "adj matrix: " << endl;
	for ( i = 0; i < size(); i++){
	  //string * l = matrix[i];
	  char c;
	  for(j=0; j< size(); j++){
	    c = getLabel(i,j);
	    cout << (c-EMPTY_EDGE) << " ";
	  }
	  cout << endl;
	}
	//cout << "occurrences size " << ocsize << endl;
	
	
	cout << "Adj list are " << endl;
	for(i=0; i< size(); i++){
		vector<GNSIZE> * t = SAdjList[i];

		cout << "at node " << i << "\t";
		for( j=0; j< t->size(); j++){
			cout << (*t)[j] << "  " ;
		}
		cout << "\n";
	}
	cout << endl;

	
#ifdef SAVERESULTS
	cout << "tid set " << endl;
	for( i=0; i<TIDs.size(); i++){
		cout << hex << TIDs[i] << " " << dec << endl;
	}
#endif
}


//find the second to last bit's index
void AdjMatrix :: last2bitil(int & bi, int & bl)
{	
	int s = size();
	char c;
	int fi = 0, i;

	for( i= (s-2); i >= 0; i--){
		c = getLabel(s-1, i);
	    //c = lr[i];
		if( c!= EMPTY_EDGE ){
			if( ++fi == 2) { bi = i, bl = (s-1); return ;}
		}
	}

	//not in the last row
	if( fi == 1){
		for( i= (s-3); i >= 0; i--){
		    c = getLabel(s-2, i);
			if( c!= EMPTY_EDGE ){
				bi = i, bl = (s-2); 
				return;
			}
		}
	}
	
	cout << " i " << i ;
	error("could not find the second to last bit");
}

void AdjMatrix::addNode(char label)
{
	int s = size(), i;

	for( i= 0; i < s; i++){
		pushLabel(EMPTY_EDGE);
	}
	pushLabel(label);
	matrixSize++;
	SAdjList.push_back(new vector<GNSIZE>);
}


void AdjMatrix::addEdge(int i, int j, GLTYPE label)
{
#ifdef SAFEMODE
	if( getLabel(i,j) != EMPTY_EDGE ) 
		error("add to a none empty edge\n");
#endif

	assignLabel(i, j, label);
	eSize++;

	//udpate the adjacency matrix
	SAdjList[i]->push_back(j);
	SAdjList[j]->push_back(i);
}

int  AdjMatrix :: esize(){

	if( eSize ) return eSize;

	int k = 0; 
	for(int i=0; i<size(); i++){
		for(int j=0; j<i; j++){
			if( getLabel(i, j) != EMPTY_EDGE)
				k++;
		}
	}
	eSize = k;
	return k;
}
void AdjMatrix::removeEdge(int i, int j)
{
	assignLabel(i, j, EMPTY_EDGE);
}

string AdjMatrix:: canonicalForm()
{
	string s; 
	int s1 = size();

	for(int i=0; i< s1; i++){
		for(int j=0; j< i; j++){
			s += getLabel(i,j);
		}
	}
	return s;
}

bool AdjMatrix:: isSubgraphOf(AdjMatrix * m, int f){

	//cout << "entering subgraph testing" << endl;
#ifdef SAVERESULTS 
	if( m->esize() < esize() ){ /*cout << "===small size" << endl;*/  return false; }   //m's size must be large

	//if( m->totalSup > totalSup ) { /*cout << "====larger suppport" << endl;*/ return false;  } //m's support value must be small
	//if( m->TIDs.size() != TIDs.size() ) error("mismatching size");
	//check the embedding relation
	/*for(int i=0; i<TIDs.size(); i++){
		if( (m->TIDs[i] & TIDs[i]) != m->TIDs[i] ){
			//cout << "====mis matching tids" << endl;
			return false;    //m's tid set is must be subsumed by subgraph's tid set
		}
	}
	
	*/
	//this->print();
	//m->print();
	
    UllmanIsomorphism UI = UllmanIsomorphism(this, m);
	//cout << "before returning" << endl;
    bool k = UI.Ullman_isomorphism();
	//cout << "leaving " << k << endl;
	return k;
#else
	return false;
#endif
}

//for validation checking, used in linux only
#ifdef VFLIB
void AdjMatrix:: setGraphForMatch(){

	if( myg == NULL){
		//cout << "buliding graph for iosmorphism testing" << endl;
		int l , s = size();
		int i, j;

		ARGEdit ed1;
		for(i=0; i<s; i++){
			l = (int) getLabel(i, i);
			ed1.InsertNode(new Point(l)); // The inserted node will have index i.                   
		}

		 // Insert the edges
		for(i=0; i<s; i++)
			for(j=0; j<s; j++){
				if (i!=j){
					l = (int) getLabel(i, j);
					//if( l != EMPTY_EDGE )
						ed1.InsertEdge(i, j, new Point(l)); // NULL stands for no sem. attribute.
			}
		}

		//set up now
		myg = new ARGraph<Point, void>(&ed1);
		myg->SetNodeDestroyer(new PointDestroyer());
		myg->SetNodeComparator(new PointComparator());
		myg->SetEdgeComparator(new PointComparator());
	}
}

bool AdjMatrix:: isSubgraph(AdjMatrix * m){

	setGraphForMatch();
	m->setGraphForMatch();
	
    // Now the Graph can be constructed...
	//UllSubState s0(&g2, myg);
	VF2SubState s0(m->getARGraph(), myg);

	int s = m->size(), i;
	int js = size() > s? size() : s;
	node_id ni1[js], ni2[js];
	int n;
	bool flag;

    if( match(&s0, &n, ni1, ni2) ){

#ifdef DEBUG5
		for( i=0; i<n; i++){
			cout << "graph small vs bigger mapping: " << ni1[i] << " -> " << ni2[i] <<endl;
		}
		cout << "end" << endl;
#endif
		return true;
	}
	else
		return false;

}

bool AdjMatrix:: isIsomorphic(AdjMatrix * m)
{
	setGraphForMatch();
	m->setGraphForMatch();

	VF2State s0(m->getARGraph(), myg);

	int s = m->size(), i;
	int js = size() > s? size() : s;
	node_id ni1[js], ni2[js];
	int n;
	bool flag;

    if( match(&s0, &n, ni1, ni2) ){

#ifdef DEBUG5
		for( i=0; i<n; i++){
			cout << "graph small vs bigger mapping: " << ni1[i] << " -> " << ni2[i] <<endl;
		}
		cout << "end" << endl;
#endif
		return true;
	}
	else
		return false;
}

bool AdjMatrix:: my_visitor(int n, node_id ni1[], node_id ni2[], void *usr_data)
{
	//FILE *f = (FILE *)usr_data;
     int * s = (int*)  usr_data;
     
	
    // Prints the matched pairs on the file
    int i;
	(*s)++;
    /*for(i=0; i<n; i++)
      fprintf(f, "(%hd, %hd) ", ni1[i], ni2[i]);
    fprintf(f, "\n");*/

    // Return false to search for the next matching
    return false;
}

int AdjMatrix:: findAllIsomophisms(AdjMatrix * m)
{

	int lsum =0;
	setGraphForMatch();
	m->setGraphForMatch();
	
	VF2SubState s0(m->getARGraph(), myg);
	match(&s0, &my_visitor, (void*) &lsum);
	
    //match(&s0, &my_visitor, NULL);
	//cout << "lsum " << lsum << endl;
	return lsum;
}

#endif

//setup the Adj list
void AdjMatrix :: buildAdjList()
{
	int s = size();
	//SAdjList = new vector<GNSIZE> *[s];
	for(int i = 0; i< s; i++){
		vector<GNSIZE> * t = new vector<GNSIZE>;
		SAdjList.push_back(t);
		for(int j =0; j< s; j++){
			if( getLabel(i,j) != EMPTY_EDGE && j!= i)	t->push_back(j);
		}
	}
}

