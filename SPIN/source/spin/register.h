#ifndef REGISTER_H
#define REGISTER_H

#include <map>
#include <string>

class Register
{
private:
	vector<long int> registers;
	int size;
	map<string, int> index;

	int get_index(string s);
public:

	Register(){ size = 0; }
	~Register(){};
	void allocate_register(string s);
	void peg(string s);
	void peg(string s, int n){ for (int i=0; i< n; i++) peg(s); }
	long int read(string s);
	void print();

};
#endif
