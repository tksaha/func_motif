#pragma warning (disable:4786 4018 4267)
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

#include "myTimer.h"

int MyTimer :: get_index(string  s)
{
	int i;
	/*map<string, int> ::iterator ip = timers.begin();
	for( ip; ip != timers.end(); ip++)
		cout << "looking for " << s << "  " << ip->first << endl;*/
	if( timers.count(s))
		i = timers[s];
	else
		i = -1;
	return i;
}

void MyTimer :: clear_timer(string  s)
{
	int i = get_index(s);
	if( i>= 0 ){	
		elapsed[i] = 0.0;
		start[i] = -1.0;
	}
	else
		log("no timer for clear");
	
}
void MyTimer :: allocate_timer(string  s)
{
	if( !timers.count(s) ){ //not allocated yet
		timers[s] = size;
		elapsed.push_back(0.0);
		start.push_back(-1.0);
		size++;
		//clear_timer(s);
	}
	else
		log("timer already there", s.c_str());
}

void MyTimer :: start_timer(string  s)
{
	int i = get_index(s);
	if( i>= 0 )
		start[i] = get_currentTime();
	else
		log("no such timer to start: ", s.c_str());
}

double MyTimer :: get_currentTime()
{

#ifdef LINUX
	struct timeval e ;
	int i = gettimeofday(&e, &tz);
	double r = (e.tv_sec - zero.tv_sec) + 1.0e-6*(e.tv_usec-zero.tv_usec);
#else
	double e = GetTickCount();
	double r = (e - zero)/1000;
#endif

	return r;
}

void MyTimer :: stop_timer(string  s)
{
	double e = get_currentTime();

	int i = get_index(s);
	if( i >= 0 ) {
		if( start[i] >= 0 ) 
			elapsed[i] += e - start[i];

		start[i] = -1.0;
	}
	else
		log("no such timer to stop: ", s.c_str());	
}

double  MyTimer::value_timer(string s)
{
	int i = get_index(s);
	if( i >= 0 ) 
		return elapsed[i];
	else
		log("no such timer for evaluation: ", s.c_str());
	return 0;
}

void MyTimer::print()
{
	map<string, int> ::iterator ip = timers.begin();
	for( ip; ip != timers.end(); ip++){
		cout << "key is " << ip->first << endl;
		cout << "index is " << ip->second << endl;
		cout << "elapsed is " << elapsed[ip->second] << endl;
	}
}
