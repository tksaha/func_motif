#pragma warning (disable:4786 4018 4267)
#include <vector>
#include <string>
#include <map>
using namespace std;

#include "common.h"
#include "register.h"


int Register :: get_index(string s)
{	
	if( index.count(s) ) 
		return index[s];
	else 
		return -1;

}

void Register :: allocate_register(string s)
{
	int i = get_index(s);

	if( i < 0  ) {
		index[s] = size++;
		registers.push_back(0);
	}
	else
		log("the register has been allocated", s.c_str());
}

void Register :: peg(string s)
{
	int i = get_index(s);

	if( i >= 0  ) {
		registers[i]++;
	}
	else
		log("no such register for pegging", s.c_str());
}

long Register :: read(string s)
{
	int i = get_index(s);

	if( i >= 0  ) {
		return registers[i];
	}
	else
		log("no such register for reading", s.c_str());

	return -1;
}

void Register :: print()
{
	map<string, int>:: iterator ip = index.begin();

	for( ip = index.begin(); ip != index.end(); ip++){
		cout << "key is" << ip->first << " value is" << ip->second << endl;
	}
}


