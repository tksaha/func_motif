#ifndef PATCHILD_H
#define PATCHILD_H

#include <set>
#include <map>
#include <vector>
#include <list>

#include "common.h"
#include "adj_matrix.h"
#include "cost1.h"
#include "occur.h"
#include "gBase.h"

#define TURNON(x, y) ((x)[(y) /32]) |=  ( 1 <<  (y) % 32)  
#define GETTID(x, y) ((x)[(y) /32]) &  ( 1 <<  (y) % 32)  
#define OUTPUTIID(x) { for(int ii=0; ii< (x).size(); ii++) cout << hex << (x)[ii] << dec << " " ; cout << endl; }

typedef map<int, vector<occur*> *, greater<int> > EDGES;
typedef vector<occur *>  OCCS;

/*
VERY IMPORTANT: optimal:   1bit: optimal or not 2 bit: maximal or not (1)   3 bit: closed or not(1);
*/

#define SCANOCCURRENCES 	int edgeSize = M->esize(), j;  \
	pattern * currp = this; \
	vector<int> gindices(edgeSize, INULLINDEX); \
	vector<short> checkpoint(edgeSize+1, 0); \
	vector<COCCS*> coccslist; \
	int fkey, skey, fkey1, gi, level, i; \
	for( i=0; i<= edgeSize; i++){ \
	if( THIRDKEY((*(currp->coccs))[0]) != GNULLINDEX ){ \
	    checkpoint[i] = 1; }\
		coccslist.push_back(currp->coccs); \
		currp = currp->par; \
	} \
	vector<GNSIZE> filter(size, 0); \
	COCCS :: iterator ip = coccs->begin(); \
	for( i=0; ip != coccs->end(); i++ , ip++){\
		fkey = FIRSTKEY((*coccs)[i]), skey = THIRDKEY((*coccs)[i]); \
		gi = SECONDKEY((*coccs)[i]); \
		level = size-1; if( checkpoint[0] ){	filter[level--]= skey; } \
		for( j=0; j< edgeSize && fkey != gindices[j]; j++){ \
			gindices[j] = fkey;  \
			fkey1 = FIRSTKEY((*(coccslist[j+1]))[fkey]);  \
			if( checkpoint[j+1]){   \
				filter[level--]= THIRDKEY((*(coccslist[j+1]))[fkey]); } \
			fkey = fkey1; \
		} 

#define GETMAXIMALBIT(x)  ((x) & 4)
#define SETMAXIMALBIT(x)  ((x) |= 4)
#define GETCLOSEDBIT(x)   ((x) & 2)
#define SETCLOSEDBIT(x)   ((x) |= 2)
#define FREFIRSTKEY(x)    ( (x) >> 24 )
#define FRESECONDKEY(x)   ( ((x) >> 16) & 0x000000ff)
#define FRETHIRDKEY(x)    ( ((x) >> 8)  & 0x000000ff)
#define FREFORTHkEY(x)    ( (x) & 0x000000ff)
#define FREMAKEKEY(x, y, z, d) ( ((x) << 24) | ((y) << 16) | ((z) << 8) | (d) )

class CompPos
{
public:
        bool operator()( IIDTYPE * const & oc1,  IIDTYPE * const & oc2);
};

class CompSup
{
public:
        bool operator()( IIDTYPE * const & oc1,  IIDTYPE * const & oc2);
};

class pattern
{
private:
	AdjMatrix			* M;
	pattern				* par;
	char				optimal;
	char				stage;
	short				size;
	cost1				mycost;
	static gBase		* gb;
	static MyTimer		* Timers;
	static Register		* Registers;
	vector<pattern *>	child;
	vector<pattern *>	newchild;
	GRAPHS1				gids;
	COCCS				* coccs;

#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
	FREELE              innerCandidates, innerOuterCandidates, outerCandidates;
	//IIDTYPE             myTids;
#endif

	//funcions for proposing child
	void fsm2( vector<AdjMatrix * > & result, int freq, int level, int & total, int &total2);
	bool proposeOuterChild(int f, vector<pattern*> & outChild);
    void scan(map<int, pattern*, greater<int> > & cand, int f, vector<pattern*> & result);
	void fixTRA();

	//used for initialization
	pattern * setUpChild(vector<int> u, EDGES & cm, EDGES & gnodes, int nlabel);
	void setUpEdges(pattern * p2, OCCS * es, TRA & tra);
	
	//used by join two patterns
	void join(pattern * p2, int freq);
	bool join1(pattern * p2, pattern * p3, int type, int freq, bool mf);
	bool propose_inner(pattern * p2, pattern * p3, int freq);
	bool propose_inner_outer(pattern * p2, pattern * p3, int freq);
	bool propose_outer2(pattern * p2, pattern * p3, int freq);
	void populate_child( pattern * p3, vector<int> * s3, int t, occur * oc1);

#ifdef MAXIMALPATTERN
	bool isMaximal();
	bool isMaximal(AdjMatrix * CM1, IIDTYPE * local, int freq );
	bool isMaximal(int t, bool all, IIDTYPE * Tid =NULL);
	bool countSuperGraph(vector<GNSIZE> & nodes, int gi, CGOCC & counter, int threshold);
#endif

	//functinos for find all isomorphism
#ifdef GSPANFLAG
	void findAllIsomorphisms();
#endif

	//save results
#ifdef SAVERESULTS
	void recordOccs(vector<occur*> & loccs);
#endif

	//others 
	void constructM (void);
	void fixM(void);
	int  isCarnonicalForm(void);
	
	//some interface between patterns
	inline int  sup() const { /*return graphs->size();*/ return gids.size(); } 
	inline int  getSize() { return size; }
	inline int  getCost() { return mycost.intValue(); }
	void sprint(int level = 0);

	//related Tree based scanning
	void frequentElement(vector<AdjMatrix*> & result, int freq, int & totalnum);
	void enuFreqEle(FREELE & candidates, vector<AdjMatrix*> & results, int freq, int & totalnum);
	int  scanElements(ELEFRENQC  & counter, int freq, vector<int> & ref);
	void addInstances(ELEFRENQC & counter, vector<GNSIZE> & occ, int gi, int instanceID, vector<int> & ref );
	int  getSupport( IIDTYPE * iid);
	int  getSupport( IIDTYPE * ele1, IIDTYPE * ele2);
	bool depthSearch(AdjMatrix * CM, IIDTYPE * Tids, FREELE & candidates, vector<AdjMatrix*> & results , int freq, int & tn);
    void intersectInstances(IIDTYPE * ele1, const IIDTYPE * ele2);
	void obtainSupportVector( IIDTYPE * iid, GRAPHS1 & gids);
	int  isTreeClosed();
	bool makeCompack(IIDTYPE * ele, FREELE & dest, int freq);
	void sortCandidates(FREELE & candidates1, FREELE & candidates2);
	bool lookAhead(AdjMatrix * CM, const IIDTYPE * Tids, const FREELE & candidates, 
						   vector<AdjMatrix*> & results , int freq, int & totalnum);

public:

	//constructor
	pattern()
	{
		M = NULL;			coccs = new COCCS();			par = NULL;			
		stage = 0;			size = 0;			   		    optimal = 0;	
	}
	
	pattern( pattern * p1, int k, int s1) {
		M = p1->M;			coccs = new COCCS();			
		par =p1;			mycost.setCost(k);				stage = 0;						
		size = s1;			gb = p1->gb;					optimal = 0;	
#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
		//myTids.reserve( (p1->coccs)->size()+31/32);
#endif
	}

	//deconstructor
	~pattern(){
#ifdef SAVERESULTS
		if( stage == 1 || stage == 2 || stage == 3 ) delete M;
#else
		if( stage == 1 || stage == 2 || stage == 3 || stage == 4)       	delete M;
#endif
		
		//delete the coccs and occs
		delete coccs;

#if (defined MAXIMALPATTERM ||  defined TREEBASEDSCAN )
		FREELE   ::iterator ip = innerCandidates.begin();
		for( ; ip != innerCandidates.end(); ip++) delete (*ip);
		for( ip = outerCandidates.begin(); ip != outerCandidates.end(); ip++)
			delete (*ip);
		for( ip = innerOuterCandidates.begin(); ip!= innerOuterCandidates.end(); ip++) 
			delete(*ip);
#endif

	}

	//member functions
	static void initPattern(gBase *gb1, MyTimer & t, Register & mr);
	void setChild(EDGES & cm, EDGES & nm);
	int fsm1( vector<AdjMatrix * > & result, int freq, int level);
	void print();

};

typedef vector<pattern *> CHLTYPE ;

#endif

