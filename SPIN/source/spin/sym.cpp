#pragma warning (disable:4786 4018 4267)
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <ctime>
#include <iomanip>
#include <errno.h>
#include <stdio.h>

using namespace std;

#include "common.h"

#ifdef LINUX
	#include <sys/time.h>
#else
	#include <windows.h>
#endif

#include "myTimer.h"
#include "register.h"
#include "pattChild.h"
#include "induPatt.h"
#include "gBase.h"

void setTimer(MyTimer & t);
void setRegister(Register & myregister);
void statistics(MyTimer & Timers, Register & myregister);
//void populateChild(children * & c, map<int, vector<occur*> *> & cm);
#ifdef VALIDATION
typedef map<int, vector<AdjMatrix*> *> INNER;
typedef map<int, INNER * > OUTER;
void frequency_checking(vector<AdjMatrix*> & r, gBase * gb );
void count(AdjMatrix * m, gBase * gb, vector<GNSIZE> & oc);
void redudancy_checking(vector<AdjMatrix*> & r);
void verify(OUTER & out);
void verify_inner(vector<AdjMatrix *> * v, int &k );
#endif

void validation( vector<AdjMatrix*> & r, gBase * gb );
void result_output(vector<AdjMatrix*> & result, string f1, string f2, string f3, 
				   vector< int > & g_reindex);
void obtainMaximalPattern(vector<AdjMatrix*> & result);
void obtainExeclusiveMaximalPattern(vector<AdjMatrix*> & result);

int main (int ARGC, char * ARGV[]){
	
	string f1, f2, f3, f4, f5;
	float freq;

#ifdef LINUX
	if (ARGC < 7) {
	   cout <<  "sym nodeFile edgeFile outNodeFile outEdgeFile outFeatureFile support\n";
       return 1;
	}
	f1 = ARGV[1];
	f2 = ARGV[2];
	f3 = ARGV[3];
	f4 = ARGV[4];
	f5 = ARGV[5];
	//float freq = 0.025;
	freq = atof( (char*)ARGV[6]);
#else
	f1 = "n7.txt";
    f2 = "e7.txt";
	//f1 = "cm_atom";
	//f2 = "cm_bond";
	f3 = f1 + "_out";
	f4 = f2 + "_out";
	f5 = f3 + "_f";
    freq = 1;
#endif
	

	//test for canonical form
    /*
	AdjMatrix testm, testm1;
	testm.addNode(EMPTY_EDGE+4); 	testm.addNode(EMPTY_EDGE+3);  testm.addNode(EMPTY_EDGE+3);testm.addNode(EMPTY_EDGE+3);
	testm1.addNode(EMPTY_EDGE+4); 	testm1.addNode(EMPTY_EDGE+3);  testm1.addNode(EMPTY_EDGE+3);testm1.addNode(EMPTY_EDGE+3);
	testm.addEdge(1, 0, EMPTY_EDGE+2); testm.addEdge(2, 0, EMPTY_EDGE+2); 
	testm.addEdge(2, 1, EMPTY_EDGE+1); testm.addEdge(3, 1, EMPTY_EDGE+1);
	testm.addEdge(3, 2, EMPTY_EDGE+1); 
	testm1.addEdge(1, 0, EMPTY_EDGE+2); //testm1.addEdge(2, 0, EMPTY_EDGE+2); 
	testm1.addEdge(2, 1, EMPTY_EDGE+1); testm1.addEdge(3, 1, EMPTY_EDGE+1);
	testm1.addEdge(3, 2, EMPTY_EDGE+1); 
	
	testm.buildAdjList(); testm1.buildAdjList(); 
	testm.print(); testm1.print();
	bool res = testm1.isSubgraphOf(&testm, 0);
	//int res = testm.isTreeBasedCanonicalForm();
	cout << "is  " << res << endl;
	return 1;
	*/
	/*
6 2 1 1 0 0 0 0 
2 6 0 0 1 0 0 0 
1 0 6 0 0 2 0 0 
1 0 0 6 0 0 2 0 
0 1 0 0 6 0 0 2 
0 0 2 0 0 6 0 0 
0 0 0 2 0 0 6 1 
0 0 0 0 2 0 1 6 
*/

	//return 0;

    cout << "=========Welcome using SPIN: maximal frequent subgraph mining=========" << endl;
	cout << "Developed by the University of North Carolina at Chapel Hill" << endl;

	//set up timer
	MyTimer Timers;
	setTimer(Timers);

	//set up registers
	Register myregister;
	setRegister(myregister);

	gBase * gb = new gBase(f1, f2, freq);
	cout << "graph database size " << gb->size();

	//construct the initial pattern
	map<int, vector<occur*> *, greater<int> > cm = gb->getMap();
	map<int, vector<occur*> *, greater<int> > nm = gb->getNodeMap();
	//children *c = new children(p);

	vector<AdjMatrix * > result;
	int ts = gb->getThreshold();
	if( ts < 1) ts = 1;
	cout << " threshold " << ts << endl;

	//start the search
	//minimal support
	if( ts  < 1 ) ts = 1;

	/*for( int i =0; i < gb->size(); i++){
		gb->graph(i)->print();
	}*/

#ifdef INDUCEDS
	iPattern * p = new iPattern(gb, Timers, myregister, cm, nm);
	//gb->deleteMap();
	//gb->deleteNodeMap();
	
	Timers.start_timer(TIMER1);
	int totalPatterns = p->fsm1(result, ts, 0 /*,  gb->getGIndex()*/);
	
#else
	//pattern * p = new pattern(gb, Timers, myregister, cm, nm);
	pattern * p = new pattern();
	p->initPattern(gb, Timers, myregister);
	p->setChild( cm,  nm);
	
	Timers.start_timer(TIMER1);
	int totalPatterns = p->fsm1(result, ts, 0 /*,  gb->getGIndex()*/);
#endif

	Timers.stop_timer(TIMER1);
	cout << "Total frequent patterns: " << dec << totalPatterns << "\t";
	cout << "Search Time "  << Timers.value_timer(TIMER1) << " seconds" << endl;

	 //get the maximal patterns
	 Timers.start_timer(TIMER1);

	 int totalTree = 0, totalPath=0;
	 for(int i=0; i< result.size(); i++){
		totalTree += result[i]->isTree();
        totalPath += result[i]->isPath();
		//result[i]->print();
	 }
	 cout << "total number of trees " << totalTree << "paths " << totalPath << " total pattern " << result.size() << endl;

	 //return 1;

	 //output statistics
	 statistics(Timers, myregister);

	 //validation
	 //validation(result, gb);

	 //obtainMaximalPattern(result);
	 Timers.stop_timer(TIMER1);
	 //cout << "Post processing time "  << Timers.value_timer(TIMER1) << " seconds" << endl;
	 //obtainExeclusiveMaximalPattern(result);
	
	//for result output
	result_output(result, f3, f4, f5, gb->getGIndex());

#ifdef TRAINING
#ifdef VALIDATION
	f1 = "test_atom";
    f2 = "test_bonds";
	gBase * tr = new gBase(f1, f2, 0.0);

	cout << "start testing set output" << endl;
	ofstream of1("test.graph");
	int i, j;
	for(i=0; i< result.size(); i++){
		for(j=0; j< tr->size(); j++){
			AdjMatrix * m = result[i];
			if( tr->isSubgraph(j, m) ){
				of1 << "fragment " << i << "  " << j << endl;
			} 		
		}
	}
	of1.flush();
	of1.close();
	cout << "end" << endl;
#endif
#endif

	return 0;

}

void statistics(MyTimer & Timers, Register & myregister)
{
	//timer related
	cout << "statistics: (if TIMERFLAG or REGISTERFLAG is turned on)" << endl;
#ifdef TIMERFLAG
	string timers[] = {TIMER1, TIMER2, TIMER3, TIMER4,TIMER5, 
		TIMER6,TIMER7, TIMER8, TIMER9, TIMER10};
	for( int i=0; i< 10; i++){
		cout << timers[i] << "  " << Timers.value_timer(timers[i].c_str()) << endl;
	}
#endif

#ifdef REGISTERFLAG
	//Timers.print();
	cout << "total children proposing tried " << myregister.read(REGISTER1) << endl;
	cout << "total children instances proposed " << myregister.read(REGISTER2) << endl;
	cout << "total children proposed " << myregister.read(REGISTER3) << endl;
	cout << "total children in carnonical form " << myregister.read(REGISTER4) << endl;
	cout << "total children in sub carnonical form " << myregister.read(REGISTER5) << endl;
	cout << "total children NOT in carnonical form " << myregister.read(REGISTER6) << endl;
	cout << "total children not frequent " << myregister.read(REGISTER7) << endl;
	cout << "total children extended " << myregister.read(REGISTER8) << endl;
	cout << "total ext children proposed " << myregister.read(REGISTER9) << endl;
	cout << "total ext children in carnonical form " << myregister.read(REGISTER10) << endl;
	cout << "total ext children in sub carnonical form " << myregister.read(REGISTER11) << endl;
	cout << "total ext children NOT in carnonical form " << myregister.read(REGISTER12) << endl;
	cout << "total ext children not frequent " << myregister.read(REGISTER13) << endl;
#endif
}

void setRegister(Register & myregister)
{
	myregister.allocate_register(REGISTER1);
	myregister.allocate_register(REGISTER2);
	myregister.allocate_register(REGISTER3);
	myregister.allocate_register(REGISTER4);
	myregister.allocate_register(REGISTER5);
	myregister.allocate_register(REGISTER6);
	myregister.allocate_register(REGISTER7);
	myregister.allocate_register(REGISTER8);
	myregister.allocate_register(REGISTER9);
	myregister.allocate_register(REGISTER10);
	myregister.allocate_register(REGISTER11);
	myregister.allocate_register(REGISTER12);
	myregister.allocate_register(REGISTER13);
	//myregister.print();
}

void setTimer(MyTimer & Timers){

	//automatic initialize the timer after allocation
	string timers[] = {TIMER1, TIMER2, TIMER3, TIMER4,TIMER5, 
		TIMER6,TIMER7, TIMER8, TIMER9, TIMER10};
	for(int i=0; i< 10; i++)
		Timers.allocate_timer(timers[i]);
}

void validation( vector<AdjMatrix*> & r, gBase * gb)
{

	
#ifdef VALIDATION
    cout << "start frequency check: " << r.size() << endl;
	frequency_checking(r, gb);
    cout << "complted"  << endl;

	//return;

	cout << "start redundancy check" << endl;
	redudancy_checking(r);
	cout << "complted"  << endl;

#endif
  

}

#ifdef VALIDATION
void frequency_checking(vector<AdjMatrix*> & r, gBase * gb )
{
	for(int i =0; i< r.size(); i++){
		AdjMatrix * m = r[i];
		vector<GNSIZE> oc;
		count(m, gb, oc);
		vector<GNSIZE> & oc1 = m->getGraphIDs();

		if( oc.size() != oc1.size() ){
			cout << "matrix size" << m->size() << " size 1 " << oc.size() << " size 2 " << oc1.size() <<  endl;
                        m->print();			
			error("not match");
                }
		else{
			if( (i+1) % 10 == 0) {
				cout << ".";
				if ( (i+1) % 200 == 0 ) cout << endl;
				cout.flush();
			}
			vector<GNSIZE> :: iterator ip, ip1;
			for( ip = oc.begin(), ip1 = oc1.begin(); ip != oc.end() && ip1 != oc1.end(); ip++, ip1++){
				if( *ip != *ip1 ){
					cout << "mismatching " << *ip << " " << *ip1 << endl;
					error("mismatching");
				}
			}
		}

	}
}

void redudancy_checking(vector<AdjMatrix*> & r)
{

	//set up the data structure
	OUTER out;
	int i=0; 
	for( i=0; i< r.size(); i++){
		AdjMatrix * m = r[i];
		vector<AdjMatrix *> * v;
		INNER * inr; 
		int s = m->size(), es = m->esize();

		if( !out.count(s) ){
			v = new vector<AdjMatrix*>();
			v->push_back(m);
			inr = new INNER();
			(*inr)[es] = v;
			out[s] = inr;
		}
		else{
			inr = out[s];
			if( !inr->count(es) ){
				v = new vector<AdjMatrix*>();
				v->push_back(m);
				(*inr)[es] = v;
			}
			else{
				v = (*inr)[es];
				v->push_back(m);
			}
		}
	}

	//verify the structure
	verify(out);
}

void verify(OUTER & out)
{
	typedef map<int, vector<AdjMatrix*> *> INNER;
	typedef map<int, INNER * > OUTER;

	OUTER::iterator ip ;

	int k =0;
	for( ip = out.begin(); ip != out.end(); ip++){
		INNER * inr = ip->second;
		INNER::iterator ip1;

		for( ip1 = inr->begin(); ip1 != inr->end(); ip1++){
			vector<AdjMatrix *> * v = ip1->second;

		
			verify_inner(v, k);
		}
	}
}

void verify_inner(vector<AdjMatrix *> * v, int &k )
{

	for(int i=0; i<v->size(); i++){		
		
		if( ++k % 10 == 0){
			cout <<"." ;
			if( k % 200 == 0) cout << endl;
			cout.flush();
		}

		AdjMatrix * m1 = (*v)[i];
		for(int j=i+1; j<v->size(); j++){
			AdjMatrix * m2 = (*v)[j];
			if( m1->isIsomorphicOf(m2) ){
				m1->print();
				m2->print();
				error("redundancy");
			}
		}
	}
}

void count(AdjMatrix * m, gBase * gb, vector<GNSIZE> & oc){
	
	for( int i = 0; i< gb->size(); i++){
        if( m->isSubgraphOf(gb->graph(i), 0) )
			oc.push_back(i);
	}
}

#endif

void result_output(vector<AdjMatrix*> & result, string f1, string f2, string f3, 
				   vector< int > & g_reindex)
{

	string files[] = {f1, f2, f3};
	int i;

#ifdef LINUX
	string com = "rm ";
#else
	string com = "del ";
#endif
	
	string com1; 
	FILE * fh;
	for( i=0; i< 3; i++){
		fh = fopen (files[i].c_str(), "r");
		if ( fh!= NULL || (fh == NULL && errno != ENOENT ) ){	
			if( fh) fclose(fh);
			com1 = com + files[i];
			system(com1.c_str());
			//cout << " command " << com1.c_str() << endl;
		}
	}

	//cout << GFILE << PFILE << endl;
	cout <<"start output result" << endl;
	for( i=0; i< result.size(); i++){
		AdjMatrix * m = result[i];

#ifdef SAVERESULTS
		m->print(i, f1, f2, f3, g_reindex); 
#else
		m->print();
#endif
	}

	cout << "end" << endl;

}

void obtainMaximalPattern(vector<AdjMatrix*> & result)
{
	vector<AdjMatrix*> lb;

	if( result.size() > 0)	lb.push_back(result[0]);

	cout << "total pattern " << result.size() << endl;
	//check the maximal pattern
	for(int i=1; i< result.size(); i++){
		AdjMatrix * t = result[i];

		if( !( (i+1) % 100 ) ) cout << ".";
		if( !( (i+1) % 1000 ) ) cout << i << endl;

		bool flag = true;
		for(int j=0; j< lb.size() && flag; j++){
			//lb[j]->print();
			flag = !(t->isSubgraphOf(lb[j], 0));
#if (defined SAFEMODE && defined VFLIB)
			//cout << "double check " << lf << endl;
			if( flag == lb[j]->isSubgraph(t) ){
				cout << "is subgraph " << lf << "\t0: no";
				error("subgraph mismatch");
			}
#endif
		}
		if( flag ){ /*cout << "t is maximal " << endl; */ lb.push_back(t); }
		else{ /* cout << "t is not maximal " << endl;*/ delete t; }
	}
	
    result.clear();
	swap(result, lb);
	cout << "total maximal pattern " << result.size() << endl;
}

void obtainExeclusiveMaximalPattern(vector<AdjMatrix*> & result)
{

	vector<AdjMatrix*> lb;
	cout << "total pattern " << result.size() << endl;
	for(int i=0; i< result.size(); i++){
		AdjMatrix * t = result[i];
		//t->print();
		bool flag = true;
		for(int j=0; j < result.size(); j++){
			//cout << "j is " << j << endl;
			if( i != j && t->isSubgraphOf(result[j],0) ){
					flag = false; break;
			}
		}
		if( flag) lb.push_back(t);
	}
	result.clear();
	swap(result, lb);
	cout << "total maximal pattern " << result.size() << endl;
}

