import rmsd 
import os
import sys 
import numpy as np 


file_list = []
normal_rmsd_list = []
kabsch_rmsd_list = []
quaternion_rmsd_list = []

for root, dirs, files in os.walk(sys.argv[1], topdown=False):
    for name in files:
        if ".pdb" in name: 
          file_list.append(os.path.join(root, name))


count = 0 
for X in file_list:
  for Y in file_list:
    if X == Y: 
       continue 

    p_atoms, p_all = rmsd.get_coordinates(X, 'pdb')
    q_atoms, q_all = rmsd.get_coordinates(Y, 'pdb')

    try: 
      if  np.count_nonzero(p_atoms != q_atoms):
          raise("Atoms not in the same order")
    except Exception as e:
          print (e)
          print (X)
          print (Y)  
          continue


    P = p_all
    Q = q_all

    try: 
      not_hydrogens = np.where(p_atoms != 'H')
      P = p_all[not_hydrogens]
      Q = q_all[not_hydrogens]
    except Exception as e:
      continue

    try: 
      normal_rmsd = rmsd.rmsd(P, Q)
      Pc = rmsd.centroid(P)
      Qc = rmsd.centroid(Q)
      P -= Pc
      Q -= Qc
      kabsch_rmsd = rmsd.kabsch_rmsd(P, Q)
      quat_rmsd = rmsd.quaternion_rmsd(P,Q)
    except Exception as e:
      pass 

    kabsch_rmsd_list.append(kabsch_rmsd)
    quaternion_rmsd_list.append(kabsch_rmsd)
    normal_rmsd_list.append(normal_rmsd)
   
    
    count = count + 1 
    print ("Iteration : {}".format(count))

print ("Calculating statistics")
from scipy import stats
print (len(normal_rmsd_list))
print (stats.describe(normal_rmsd_list))
print (stats.describe(kabsch_rmsd_list))
print (stats.describe(quaternion_rmsd_list))



