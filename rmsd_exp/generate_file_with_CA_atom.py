import os 
import sys 
import shlex
import subprocess


file_list = []



for root, dirs, files in os.walk(sys.argv[1], topdown=False):
    for name in files:
        if ".pdb" in name: 
          file_list.append(os.path.join(root, name))


output_dir = sys.argv[2]
for file_ in file_list:
	p = subprocess.Popen(shlex.split('grep ATOM.*CA.*C {}'.format(file_)), stdout=subprocess.PIPE)
	out, err = p.communicate()	
	output_file = open('{}{}'.format(output_dir,file_[file_.rfind("/"): ]), "w")
	output = out.decode('ascii')

	count = 1 
	total_lines = len(list(output.split("\n")))

	for line in output.split("\n"):
		output_file.write (line)
		if  count < total_lines:
			output_file.write(os.linesep)
			count = count + 1
		
	output_file.close()


